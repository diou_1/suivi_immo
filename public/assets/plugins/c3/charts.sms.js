(function($) {
  'use strict';
  var rel = $("#table-result").attr('rel');
  var c3DonutChart = c3.generate({
    bindto: '#sms-donut-chart',
    data: {
      columns: [
        [rel, 100],
      ],
      type: 'donut',
      onclick: function(d, i) {
        console.log("onclick", d, i);
      },
      onmouseover: function(d, i) {
        console.log("onmouseover", d, i);
      },
      onmouseout: function(d, i) {
        console.log("onmouseout", d, i);
      }
    },
    color: {
      //pattern: ['rgba(88,216,163,1)', 'rgba(4,189,254,0.6)', 'rgba(237,28,36,0.6)']
      //pattern: ['rgba(255,51,51)', 'rgba(38,196,236)', 'rgba(0,128,128)']
      pattern: ['rgb(192, 192, 192)', 'rgba(0,128,128)', 'rgba(38,196,236)']
    },

    padding: {
      top: 50,
      right: 0,
      bottom: 50,
      left: 0,
    },
    donut: {
      //title: "VISA"
    }
  });

  var res = $("#sms").attr("data-chart");
  var obj = jQuery.parseJSON(res);

  var trente_jrs = ["30 jrs("+obj._30jrs+")", obj._30jrs];
  var soixante_jrs = ["60 jrs("+obj._60jrs+")", obj._60jrs];
  var quatre_vingt_dix_jrs = ["90 jrs("+obj._90jrs+")", obj._90jrs];

  var cols = [];

  if(obj._30jrs>0) {
    cols.push(trente_jrs);
  }
  if(obj._60jrs>0) {
    cols.push(soixante_jrs);
  }
  if(obj._90jrs>0) {
    cols.push(quatre_vingt_dix_jrs);
  }

  if(obj._30jrs>0 || obj._60jrs>0 || obj._90jrs>0){
      setTimeout(function() {
        c3DonutChart.load({
          columns: cols
        });
      }, 1500);

      setTimeout(function() {
        c3DonutChart.unload({
          ids: rel
        });
      }, 1500);
  }


})(jQuery);
