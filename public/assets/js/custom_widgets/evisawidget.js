$(document).ready(function (ev) {
    $.widget('custom.evisawidget', {
        options: {
            modifId: {
                container: 'table',
                isDelete: false
            },
            modalContainer: {
                container: 'body',
                isModal: false,
                url: 'data-url',
                initButton: '#'
            },
            table: '#',
            datatable : true
        },
        _create: function (options) {
            if (typeof options !== 'undifined' && options !== null) {
                $.extend({}, this.options, options);
            }
        },
        _init: function () {
            if (this.options.modalContainer.isModal) {
                this.initFilling();
            }
            this.getAll();
        },
        deleteAlert: function () {
            var app = this;
            $(app.options.modifId.container).off('click').on('click', function (ev) {
                var url = $(this).attr('data-url');
                swal({
                    title: "Etes Vous sur?",
                    text: "Appuyez sur OK pour valider!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#37dd26",
                    confirmButtonText: "OK",
                    cancelButtonText: "Annuler!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.post(url, function (res) {
                            if (res.state == "success") {
                                swal("Succes!", res.message, res.state);
                                app.getAll();
                            } else {
                                swal("Erreur!", res.message, res.state);
                                app.getAll();
                            }
                        });
                    }
                });
            });
        },
        getAll: function () {
            var app = this;
            $.get($(app.element).attr('data-url'), function (res) {
                console.log(res.state);
                if (res.state == "success") {

                     if (app.options.datatable) { 
                         $(app.element).find('table tbody').empty();
                         $(app.element).find('table tbody').append(res.html);
                     } else {
                         $(app.element).empty();
                         $(app.element).append(res.html);
                     }

                } else {
                    swal("Erreur!", res.message, res.state);
                }
            });
        },
        fillModal: function () {
            var app = this;
            $.post($(app.options.modalContainer.container).attr(app.options.modalContainer.url), function (res) {
                if (res.state == "success") {
                    $(app.options.modalContainer.container).find('.modal-content').empty();
                    $(app.options.modalContainer.container).find('.modal-content').append(res.html);
                } else {
                    swal("Erreur!", res.message, res.state);
                }
            });
        },
        initFilling: function () {
            var app = this;
            $(app.options.modalContainer.initButton).off('click').on('click', function () {
                app.fillModal();
                $(app.options.modalContainer.container).modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $(app.options.modalContainer.container).modal('show');
            });
        }
    });
});
