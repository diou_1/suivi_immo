$(document).ready(function (ev) {
    $.widget('custom.searchwidget', {
        options: {
            targetTable: '#',
            searchButton: '#',
            form: '#',
            printer: false
        },
        _create: function (options) {
            if (typeof options !== 'undifined' && options !== null) {
                $.extend({}, this.options, options);
            }
        },
        _init: function () {
            if (!this.options.printer) {
                this.initTable();
                this.search();
            }
        },
        search: function () {
            var app = this;
            $(app.options.searchButton).off('click').on('click', function () {
                $(app.options.targetTable).empty();
                //$(app.options.targetTable).attr('rel', 'chargement...');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).empty();
                        $(app.options.targetTable).append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
            $(document).keypress(function (ev) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    var url = $(app.options.form).attr('data-url');
                    $.post(url, $(app.options.form).serialize(), function (data) {
                        if (data.state == 'success') {
                            $(app.options.targetTable).empty();
                            $(app.options.targetTable).append(data.html);
                        } else {
                            swal('Erreur!', data.message, data.state);
                        }
                    });
                    return false;
                }
            });
        },
        initTable: function () {
            var app = this;
            $.get($(app.element).attr('data-url'), function (res) {
                if (res.state == "success") {
                    $(app.options.targetTable).empty();
                    $(app.options.targetTable).append(res.html);
                } else {
                    swal("Erreur!", res.message, res.state);
                }
            });
        }
    });
});
