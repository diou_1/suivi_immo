$(document).ready(function (ev) {
    $.widget('custom.searchwidget', {
        options: {
            targetTable: '#',
            searchButton: '#',
            crd: '#',
            mens: '#',
            enc: '#',
            par: '#',
            dec: '#',
            form: '#',
            printer: false
        },
        _create: function (options) {
            if (typeof options !== 'undifined' && options !== null) {
                $.extend({}, this.options, options);
            }
        },
        _init: function () {
            if (!this.options.printer) {
                this.initTable();
                this.search();
                this.crd();
                this.mens();
                this.encours();
                this.partiel();
                this.decaissements();
            }
        },
        search: function () {
            var app = this;
            $(app.options.searchButton).off('click').on('click', function () {
                $(app.options.targetTable).empty();
                //$(app.options.targetTable).attr('rel', 'chargement...');
                $(app.options.targetTable).append('<div align="center">chargement en cours...</div>');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).empty();
                        $(app.options.targetTable).append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
            $(document).keypress(function (ev) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    var url = $(app.options.form).attr('data-url');
                    $.post(url, $(app.options.form).serialize(), function (data) {
                        if (data.state == 'success') {
                            $(app.options.targetTable).empty();
                            $(app.options.targetTable).append(data.html);
                        } else {
                            swal('Erreur!', data.message, data.state);
                        }
                    });
                    return false;
                }
            });
        },

        crd: function () {
            var app = this;
            $(app.options.crd).off('click').on('click', function () {
                $(app.options.targetTable).empty();
                $(app.options.targetTable).append('<div align="center">chargement en cours...</div>');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).empty();
                        $(app.options.targetTable).append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        mens: function () {
            var app = this;
            $(app.options.mens).off('click').on('click', function () {
                $(app.options.targetTable).empty();
                $(app.options.targetTable).append('<div align="center">chargement en cours...</div>');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).empty();
                        $(app.options.targetTable).append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        encours: function () {
            var app = this;
            $(app.options.enc).off('click').on('click', function () {
                $(app.options.targetTable).empty();
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).empty();
                        $(app.options.targetTable).append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        partiel: function () {
            var app = this;
            $(app.options.par).off('click').on('click', function () {
                $(app.options.targetTable).empty();
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).empty();
                        $(app.options.targetTable).append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        decaissements: function () {
            var app = this;
            $(app.options.dec).off('click').on('click', function () {
                $(app.options.targetTable).empty();
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).empty();
                        $(app.options.targetTable).append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        initTable: function () {
            var app = this;
            $(app.options.targetTable).append('<div align="center">chargement en cours...</div>');
            $.get($(app.element).attr('data-url'), function (res) {
                if (res.state == "success") {
                    $(app.options.targetTable).empty();
                    $(app.options.targetTable).append(res.html);
                } else {
                    swal("Erreur!", res.message, res.state);
                }
            });
        }
    });
});
