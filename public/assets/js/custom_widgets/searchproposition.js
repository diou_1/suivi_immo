$(document).ready(function (ev) {
    $.widget('custom.searchwidget', {
        options: {
            targetTable: '#',
            searchButton: '#',
            form: '#',
            printer: false
        },
        _create: function (options) {
            if (typeof options !== 'undifined' && options !== null) {
                $.extend({}, this.options, options);
            }
        },
        _init: function () {
            if (!this.options.printer) {
                this.search();
            }
        },
        search: function () {
            var app = this;
            $(app.options.searchButton).off('click').on('click', function () {
                var url = $(app.options.form).attr('action');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    this.proposition();
                    /*if (data.state == 'success') {
                        this.proposition();
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }*/
                    return false;
                });
            });
            $(document).keypress(function (ev) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    var url = $(app.options.form).attr('action');
                    $.post(url, $(app.options.form).serialize(), function (data) {
                        this.proposition();
                        /*if (data.state == 'success') {
                            
                        } else {
                            swal('Erreur!', data.message, data.state);
                        }*/
                    });
                    return false;
                }
            });
        },
        proposition: function () {
            if (colorName === null || colorName === '') { colorName = 'bg-green'; }
            if (placementFrom === null || placementFrom === '') { placementFrom = 'bottom'; }
            if (placementAlign === null || placementAlign === '') { placementAlign = 'center'; }
            if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
            if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
            if (text === null || text === '') { text = 'txtx Notification'; }
            var allowDismiss = true;
            $.notify({
                message: text
            },
                {
                    type: colorName,
                    allow_dismiss: allowDismiss,
                    newest_on_top: true,
                    timer: 1000,
                    placement: {
                        from: placementFrom,
                        align: placementAlign
                    },
                    animate: {
                        enter: animateEnter,
                        exit: animateExit
                    },
                    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
                });
        }
    });
});
