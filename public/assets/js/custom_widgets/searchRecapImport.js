$(document).ready(function (ev) {
    $.widget('custom.searchwidget', {
        options: {
            targetTable: '#',
            searchButton: '#',
            enc: '#',
            par: '#',
            dec: '#',
            form: '#',
            printer: false
        },
        _create: function (options) {
            if (typeof options !== 'undifined' && options !== null) {
                $.extend({}, this.options, options);
            }
        },
        _init: function () {
            if (!this.options.printer) {
                this.initTable();
                this.search();
                this.encours();
                this.partiel();
                this.decaissements();
            }
        },
        search: function () {
            var app = this;
            $(app.options.searchButton).off('click').on('click', function () {
                $(app.options.targetTable).find('tbody').empty();
                $(app.options.targetTable).find('tbody').append('<tr><td colspan="7" align="center">Chargement...</td></tr>');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).find('tbody').empty();
                        $(app.options.targetTable).find('tbody').append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
            $(document).keypress(function (ev) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    $(app.options.targetTable).find('tbody').append('<tr><td colspan="7" align="center">Chargement...</td></tr>');
                    var url = $(app.options.form).attr('data-url');
                    $.post(url, $(app.options.form).serialize(), function (data) {
                        if (data.state == 'success') {
                            $(app.options.targetTable).find('tbody').empty();
                            $(app.options.targetTable).find('tbody').append(data.html);
                        } else {
                            swal('Erreur!', data.message, data.state);
                        }
                    });
                    return false;
                }
            });
        },

        encours: function () {
            var app = this;
            $(app.options.enc).off('click').on('click', function () {
                $(app.options.targetTable).find('tbody').empty();
                $(app.options.targetTable).find('tbody').append('<tr><td colspan="7" align="center">Chargement...</td></tr>');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).find('tbody').empty();
                        $(app.options.targetTable).find('tbody').append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        partiel: function () {
            var app = this;
            $(app.options.par).off('click').on('click', function () {
                $(app.options.targetTable).find('tbody').empty();
                $(app.options.targetTable).find('tbody').append('<tr><td colspan="7" align="center">Chargement...</td></tr>');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).find('tbody').empty();
                        $(app.options.targetTable).find('tbody').append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        decaissements: function () {
            var app = this;
            $(app.options.dec).off('click').on('click', function () {
                $(app.options.targetTable).find('tbody').empty();
                $(app.options.targetTable).find('tbody').append('<tr><td colspan="7" align="center">Chargement...</td></tr>');
                var url = $(app.options.form).attr('data-url');
                $.post(url, $(app.options.form).serialize(), function (data) {
                    if (data.state == 'success') {
                        $(app.options.targetTable).find('tbody').empty();
                        $(app.options.targetTable).find('tbody').append(data.html);
                    } else {
                        swal('Erreur!', data.message, data.state);
                    }
                    return false;
                });
            });
        },

        initTable: function () {
            var app = this;
            $.get($(app.element).attr('data-url'), function (res) {
                if (res.state == "success") {
                    $(app.options.targetTable).find('tbody').empty();
                    $(app.options.targetTable).find('tbody').append(res.html);
                } else {
                    swal("Erreur!", res.message, res.state);
                }
            });
        }
    });
});
