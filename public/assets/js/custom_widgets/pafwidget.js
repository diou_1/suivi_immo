$(document).ready(function (ev) {
    $.widget('custom.pafwidget', {
        options: {
            table: '#',
            actionButton: '#',
            modal: '#',
            qrcode: {
                input: '#',
            },
            validate: '#',
            validateGlobal: '#',
            list: false,
            isModal: true,
            datatable: true,
            deleteBtn: '#',
            modifyBtn: '#',
            departure: false,
            //old visa
            visaExpiryDate: null
        },
        _create: function (options) {
            if (typeof options !== 'undifined' && options !== null) {
                $.extend({}, this.options, options);
            }
        },
        _init: function () {
            this.initModal();
            this.validateModal();
            this.getQrCodeInfo();
            this.globalValidation();
            this.deleteAlert();
            this.modifyMrz();
            if (this.options.departure) {
                this.keyupVisa();
                this.prolongation();
            }

        },
        initModal: function () {
            var app = this;
            $(app.options.actionButton).off('click').on('click', function () {
                $.post($(this).attr('action-url'), function (res) {
                    if (res.state == "success") {
                        if (app.options.isModal) {
                            $(app.options.modal).find('.modal-content').empty();
                            $(app.options.modal).find('.modal-content').append(res.html);
                            $(app.options.modal).modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            $(app.options.modal).modal('show');
                        } else {
                            swal("Succes!", res.message, res.state);
                        }
                        if (app.options.list) {
                            app.getList();
                        }

                    } else {
                        swal("Erreur!", res.message, res.state);
                    }
                });
            });
        },
        validateModal: function () {
            var app = this;
            $(app.options.validate).off('click').on('click', function () {
                var validateButton = $(this).closest('form');
                if ($(this).closest('form').validate()) {

                    $.post($(this).closest('form').attr('data-url'), $(this).closest('form').serialize(), function (res) {
                        //Old visa rm -rf
                        if (res.old === true) {
                            swal("Fait!", "Visa enregistré au depart à la date d'aujourd'hui.", "success");
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                            return false;
                        }
                        if (res.state == "success") {
                            if (res.conflict === true) {
                                swal({
                                    title: res.message + " : Etes vous sûr?",
                                    text: "Le depart sera validé à la date d'aujourd'hui!",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Oui, Enregistrer!",
                                    cancelButtonText: "Non, Annuler!",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                }, function (isConfirm) {
                                    if (isConfirm) {
                                        $.post(validateButton.attr('data-url-renew'), validateButton.serialize(), function (res) {
                                            if (res.state == "success") {
                                                if (app.options.datatable) {
                                                    $(app.element).find('table tbody').empty();
                                                    $(app.element).find('table tbody').append(res.html);
                                                } else {
                                                    $(app.element).empty();
                                                    $(app.element).append(res.html);
                                                }
                                                validateButton.find("input[type=text],input[type=number], textarea,  input[type=checkbox]").val("");
                                                swal("Fait!", "Visa enregistré au depart à la date d'aujourd'hui.", "success");
                                                $(app.options.qrcode.modal).modal('hide');
                                            } else {
                                                swal("Erreur!", res.message, res.state);
                                            }
                                        });

                                    } else {
                                        validateButton.find("input[type=text],input[type=number], textarea,  input[type=checkbox]").val("");
                                        swal("Annuler", "Contrôle au depart annulé", "info");
                                        $(app.options.qrcode.modal).modal('hide');
                                    }
                                });
                            } else {
                                if (app.options.datatable) {
                                    $(app.element).find('table tbody').empty();
                                    $(app.element).find('table tbody').append(res.html);
                                } else {
                                    $(app.element).empty();
                                    $(app.element).append(res.html);
                                }
                                validateButton.find("input[type=text],input[type=number], textarea,  input[type=checkbox]").val("");
                                $(app.options.qrcode.modal).modal('hide');
                            }
                            $(app.options.qrcode.modal).modal('hide');
                        } else {
                            swal("Erreur!", res.message, res.state);
                        }
                    });
                }
            });
        },
        getList: function () {
            var app = this;
            $.get($(app.element).attr('data-url'), function (res) {
                if (res.state == "success") {
                    $(app.element).find('table tbody').empty();
                    $(app.element).find('table tbody').append(res.html);

                } else {
                    swal("Erreur!", res.message, res.state);
                }
            });
        },
        getQrCodeInfo: function () {
            var app = this;
            $(app.options.qrcode.input).off('change').on('change', function (e) {
                e.preventDefault();
                var url = $(app.options.qrcode.input).attr('qrcode-url');
                $.post(url, { 'qrcode': $(this).val() }, function (data) {
                    if (data.state == 'success') {
                        var res = data.result;
                        $('input[name=v_visa_number]').val(res.visaNumber);
                        $('input[name=v_stay_duration]').val(res.stayDuration);
                        $('input[name=v_name]').val(res.firstname);
                        $('input[name=v_lastname]').val(res.lastname);
                        $('input[name=v_passport_number]').val(res.passport);
                        $('input[name=v_prolongation]').val(res.prolongation);
                        $('input[name=v_expiry_date]').val(res.expiryDate);
                        //old visa
                        app.options.visaExpiryDate = $('input[name=v_expiry_date]').val();
                    } else {
                        swal("Erreur!", data.message, data.state);
                        $('input[name=v_visa_number]').val("");
                        $('input[name=v_stay_duration]').val("");
                        $('input[name=v_name]').val("");
                        $('input[name=v_lastname]').val("");
                        $('input[name=v_passport_number]').val("");
                        $('input[name=v_prolongation]').val("");
                        $('input[name=v_expiry_date]').val("");
                    }
                    $('textarea[name=v_mrz]').focus();
                });
            });
        },
        addDays: function (days, issuance) {
            var from = issuance.split("-")
            var result = new Date(from[2], from[1] - 1, from[0]);
            console.log(result);
            result.setDate(result.getDate() + days);
            return this.stringDate(result);
        },
        stringDate: function (date) {
            var date = new Date(date);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; // January is 0!
            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + '-' + mm + '-' + yyyy;
            return date;
        },
        globalValidation: function () {
            var app = this;
            console.log($(app.options.validateGlobal));
            $(app.options.validateGlobal).off('click').on('click', function () {
                var form = $(this).closest('form');
                if ($(this).closest('form').validate()) {
                    $.post($(this).closest('form').attr('data-url'), $(this).closest('form').serialize(), function (res) {
                        if (res.state == "success") {
                            if (app.options.list) {
                                app.getList();
                            }
                            $(app.options.modal).modal('hide');
                        } else {
                            swal("Erreur!", res.message, res.state);
                        }
                    });
                }
            });
        },
        deleteAlert: function () {
            var app = this;
            $(app.options.deleteBtn).off('click').on('click', function (ev) {
                var url = $(this).attr('data-url');
                swal({
                    title: "Etes Vous sur?",
                    text: "Appuyez sur OK pour valider!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#37dd26",
                    confirmButtonText: "OK",
                    cancelButtonText: "Annuler!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.post(url, function (res) {
                            if (res.state == "success") {
                                swal("Succes!", res.message, res.state);
                                //app.getList();
                            } else {
                                swal("Erreur!", res.message, res.state);
                                //app.getList();
                            }
                        });
                    }
                });
            });
        },
        modifyMrz: function () {
            var app = this;
            $(app.options.modifyBtn).off('click').on('click', function () {
                var url = $(this).attr('data-url');
                $.post(url, function (data) {
                    if (data.state) {
                        var res = data.result;
                        $('textarea[name=v_mrz]').html(res.passport.mrz);
                        $('input[name=v_name]').val(res.passport.firstname);
                        $('input[name=v_lastname]').val(res.passport.lastName);
                        $('input[name=v_birthday]').val(res.passport.birthday);
                        $('input[name=v_nationality]').val(res.passport.nationality);
                        $('input[name=v_nationality_show]').val(res.passport.nationalityShow.name);
                        $('input[name=v_sex]').val(res.passport.sex);
                        $('input[name=v_passport]').val(res.passport.number);
                        $('input[name=v_pass_exp]').val(res.expirationDate);
                        $('input[name=v_date_dem]').val(res.applyDate);
                        $('input[name=v_issue_country]').val(res.passport.issueCountry);
                        $('input[name=v_issue_country_show]').val(res.passport.issueCountryShow.name);
                        $('input[name=v_obs_cp]').val(res.obsCase);
                        $('input[name=v_reason]').val(res.travelPattern);
                        $('#' + res.specificCase).prop('checked', true);
                        $('#' + res.stayDuration + 'j').prop('checked', true);
                        $(app.options.modal).modal('show');
                    } else {
                        swal("Info!", data.message, 'warning');
                    }
                    $('textarea[name=v_mrz]').focus();
                });
            });
        },
        keyupVisa: function () {
            var app = this;
            $('input[name=v_visa_number]').off('keyup').on('keyup', function () {
                var value = $(this).val();
                var url = $(this).attr('data-url');
                if (value.length === 12) {
                    $.post(url, { '_visa': value }, function (data) {
                        if (data.state == 'success') {
                            var res = data.result;
                            $('input[name=v_visa_number]').val(res.visa.number);
                            $('input[name=v_stay_duration]').val(res.visa.stayDuration);
                            $('input[name=v_name]').val(res.visa.passport.firstname);
                            $('input[name=v_lastname]').val(res.visa.passport.lastName);
                            $('input[name=v_passport_number]').val(res.visa.passport.number);
                            $('input[name=v_prolongation]').val(0);
                            $('input[name=v_expiry_date]').val(res.expiryDate);

                        } else {
                            swal("Erreur!", data.message, data.state);
                        }
                    });
                }
            });
        },
        prolongation: function () {
            var app = this;
            $('input[name=v_prolongation]').on('change', function () {
                var url = $(this).attr('data-url');
                //if oldvisa rm
                if ($('input[name=v_visa_number]').val().length === 11) {
                    var add = parseInt($('input[name=v_prolongation]').val());
                    var expiryDate = $('input[name=v_expiry_date]').val();
                    //add = (add + parseInt($('input[name=v_stay_duration]').val()) - 1);
                    var newExpiry = app.addDays(add, app.options.visaExpiryDate);
                    $('input[name=v_expiry_date]').val(newExpiry);
                } else {
                    if ($(this).val() == null || $(this).val() == "") {
                        add = 0;
                        $(this).val(add);
                    } else {
                        $.post(url, { '_visa': $('input[name=v_visa_number]').val() }, function (data) {
                            if (data.status) {
                                var add = parseInt($('input[name=v_prolongation]').val());
                                add = (add + parseInt($('input[name=v_stay_duration]').val()) - 1);
                                var newExpiry = app.addDays(add, data.expirydate);
                                $('input[name=v_expiry_date]').val(newExpiry);
                            } else {
                                swal('Erreur!', data.message, 'error');
                            }
                        });

                    }
                }

            });
        }
    });
});
