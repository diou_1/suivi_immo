$(function () {
    var map;
    var gdpData = {};
    var gdpData2 = {};
    var count = $("#world-map").attr('count');
    var iata = $("#world-map").attr('site');

    var colors = ['#008000'];
    if (iata !== null && iata !== undefined) {
        switch (iata) {
            case "TNR":
                colors = ['#008000'];
                break;
            case "NOS":
                colors = ['#2196F3'];
                break;
            case "MJN":
                colors = ['#2196F3'];
                break;
            case "DIE":
                colors = ['#00BCD4'];
                break;
            case "SMS":
                colors = ['#E91E63'];
                break;
            case "TMM":
                colors = ['#673AB7'];
                break;
            case "TLE":
                colors = ['#3F51B5'];
                break;
            case "FTU":
                colors = ['#CDDC39'];
                break;
            default:
                colors = ['#008000'];
        }
    }

    if (count > 0) {
        var res = $("#world-map").attr('rel');
        var JSONObject = JSON.parse(res);
        var k = 1;
        for (var i = 0; i < JSONObject.length; i++) {
            var visa = JSONObject[i];
            country = visa.nationalite;
            number = visa.nombre;
            if (country != null && number != null) {
                gdpData[country] = number;
                //console.log(gdpData);
                if (i < 5) {
                    gdpData2[country] = number;
                }
            } else {
                console.log('N°' + i + ' : ' + country + ':' + number);
            }
            k = k + 1;
        }

        map = new jvm.Map({
            map: 'world_mill_en',
            container: $('#world-map'),
            //scaleColors: ['#C8EEFF'],
            normalizeFunction: 'polynomial',
            hoverOpacity: 0.7,
            hoverColor: false,
            //regionsSelectable: true,
            //backgroundColor: 'transparent',
            backgroundColor: '#383f47',
            regionStyle: {
                initial: {
                    fill: 'rgba(210, 214, 222, 1)',
                    "fill-opacity": 1,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 1
                },
                hover: {
                    "fill-opacity": 0.7,
                    cursor: 'pointer'
                },
                selected: {
                    //fill: 'yellow'
                    fill: '#FF5722'
                },
                selectedHover: {}
            },
            markerStyle: {
                initial: {
                    //fill: '#009688',
                    fill: '#FF5722',
                    stroke: '#000'
                }
            },

            //-----------test GDP--------------
            series: {
                regions: [{
                    values: gdpData,
                    scale: colors,
                    normalizeFunction: 'polynomial'
                }/*,
                {
                    values: gdpData2,
                    scale: ['#FF5722'],
                    normalizeFunction: 'polynomial'
                }*/]
            },
            onRegionTipShow: function (e, el, code) {
                //el.html(el.html()+' ('+gdpData[code]+')');
                el.html(el.html());
            }

        });

        if (k > 5) {
            for (var i = 0; i < 5; i++) {
                var visa = JSONObject[i];
                country = visa.nationalite;
                number = visa.nombre;
                if (country != null && number != null) {
                    console.log(country);
                    if (jvmCountries[country].coords != null) {
                        map.addMarker(i, { latLng: jvmCountries[country].coords, name: jvmCountries[country].name + '(' + country + ')' + ' : ' + number });
                    }
                } else {
                    console.log('N°' + i + ' : ' + country + ':' + number);
                }
            }
        } else {
            for (var i = 0; i < JSONObject.length; i++) {
                var visa = JSONObject[i];
                country = visa.nationalite;
                number = visa.nombre;
                if (country != null && number != null) {
                    console.log(country);
                    if (jvmCountries[country].coords != null) {
                        map.addMarker(i, { latLng: jvmCountries[country].coords, name: jvmCountries[country].name + '(' + country + ')' + ' : ' + number });
                    }
                } else {
                    console.log('N°' + i + ' : ' + country + ':' + number);
                }
            }
        }


    } else {
        map = new jvm.Map({
            map: 'world_mill_en',
            container: $('#world-map-vide'),
            normalizeFunction: 'polynomial',
            hoverOpacity: 0.7,
            hoverColor: false,
            //backgroundColor: 'transparent',
            backgroundColor: '#383f47',
            regionStyle: {
                initial: {
                    fill: 'rgba(210, 214, 222, 1)',
                    "fill-opacity": 1,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 1
                },
                hover: {
                    "fill-opacity": 0.7,
                    cursor: 'pointer'
                },
                selected: {
                    fill: '#FF5722'
                },
                selectedHover: {}
            },
            markerStyle: {
                initial: {
                    fill: '#009688',
                    stroke: '#000'
                }
            },
            onRegionTipShow: function (e, el, code) {
                el.html(el.html());
            }

        });
    }


});

