$(function () {
    //Horizontal form basic
    $('#wizard_horizontal').steps({
        headerTag: 'h2',
        bodyTag: 'section',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
            setButtonWavesEffect(event);
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        }
    });

    //Vertical form basic
    $('#wizard_vertical').steps({
        headerTag: 'h2',
        bodyTag: 'section',
        transitionEffect: 'slideLeft',
        stepsOrientation: 'vertical',
        onInit: function (event, currentIndex) {
            setButtonWavesEffect(event);
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        }
    });

    //Advanced form with validation
    var form = $('#wizard_with_validation').show();
    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        labels: {
            pagination: "Pagination",
            finish: "Terminer",
            next: "Suivant",
            previous: "Precedent",
            loading: "Patience ..."
        },
        onInit: function (event, currentIndex) {
            $.AdminBSB.input.activate();

            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //new register

            $('#new-register').off('click').on('click', function () {
                var table = $('#step-one').find('tr.list-one').length;
                if (table > 0) {
                    swal({
                            title: "Êtes vous sûr?",
                            text: "Un traitement est en cours dont la transaction n\'a pas été achevée,\n Annuler quand même?!",
                            type: "warning",
                            showCancelButton: true,
                            cancelButtonText: "Non, continuer",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Oui, annuler traitement en cours!",
                            closeOnConfirm: false
                        },
                        function () {
                            $.post($('#array-json').attr('data-url'),  function (data) {
                                if (data.status == true) {
                                    location.reload();
                                }
                                else {
                                    swal('Erreur!', data.message, data.state);
                                }
                            });

                        });
                } else {
                    swal('Info!', 'Ceci est une nouvelle enregistrement!', 'info');
                }
            });

            //set button waves effect
            setButtonWavesEffect(event);

        },
        onStepChanging: function (event, currentIndex, newIndex) {

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }
            switch (currentIndex) {
                case 0:
                    if (currentIndex > newIndex) {
                        return true;
                    }
                    var table = $('#step-one').find('tr.list-one').length;
                    if (table > 0) {
                        return true;
                    } else {
                        swal('Info!', 'Aucune entrée à traiter', 'info');
                        return false;
                    }
                    break;
                case 1:
                    if (currentIndex > newIndex) {
                        return true;
                    }
                    form.validate().settings.ignore = ':disabled,:hidden';
                    var res = false;
                    if (form.valid()) {
                        var url = form.attr('data-url');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: form.serialize(),
                            dataType: "json",
                            async: false,
                            success: function (data) {
                                res = data.status;
                                $(event.currentTarget).find('#download-table').find('tbody').empty();
                                $(event.currentTarget).find('#download-table').find('tbody').html(data.html);
                            },
                            error: function (resultat, statut, erreur) {
                                swal('Erreur!', erreur, statut);
                                res = false;
                            }
                        });
                        /*$.post(url, form.serialize(), function (data) {
                            return data.status;
                        });*/
                        return res;
                    }
                    break;
                case 2:
                    if (currentIndex > newIndex) {
                        swal('Page de telechargement de facture!', 'Transaction déjà validée.\n Veuillez proceder l\'annulation en cas d\'erreur ', 'info');
                        return false;
                    }
                    break;
            }

            /*  form.validate().settings.ignore = ':disabled,:hidden';
              return form.valid();*/

        },
        onStepChanged: function (event, currentIndex, priorIndex) {

            switch (currentIndex) {
                case 0:
                    break;
                case 1:
                    var visas = $('#jsonData').attr('data-result');
                    var url = $('#jsonData').attr('data-url');
                    var currency = $("input[name='v_currency']:checked").val();
                    calculate(url, visas, currency);
                    activateTpeNumber($("input[name='v_ptype']:checked"), $('#v_tpe_number'))
                    $("input[name='v_currency']").click(function (ev) {
                        currency = this.value;
                        calculate(url, visas, currency);
                        $("input[name='v_collect']").val(0);
                        $("input[name='v_collect']").keyup();
                    });
                    $("input[name='v_collect']").val(0);
                    $("input[name='v_collect']").keyup(function (ev) {
                        $("input[name='v_change']").val(parseInt(this.value) - parseInt($("input[name='v_amount']").val()));
                    });
                    $("input[name='v_ptype']").change(function (ev) {
                        activateTpeNumber($("input[name='v_ptype']:checked"), $('#v_tpe_number'))
                    });
                    break;
                case 2:

                    break;
            }

            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            // swal("Good job!", "Submitted!", "success");
            $.post($('#array-json').attr('data-url'),  function (data) {
                if (data.status == true) {
                    location.reload();
                }
                else {
                    swal('Erreur!', data.message, data.state);
                }
            });
        }
    });

    $.validator.addMethod("compareTo",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseInt(value, 10) >= parseInt($otherElement.val(), 10);
        });


    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'v_collect': {
                number: true,
                compareTo: '#v_amount'
            },
            'v_change': {
                number: true,
                min: 0
            },
            'v_tpe_number': {
                required: function (element) {
                    if ($("#tpe").is(':checked')) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        },
        messages: {
            'v_collect': {
                number: 'Ceci n\'est pas un nombre',
                compareTo: 'Le montant dû est insufisant'
            },
            'v_change': {
                number: 'Ceci n\'est pas un nombre',
                min: 'Le montant dû est insufisant'
            },
            'v_tpe_number': {
                required: 'Le numero de transaction est obligatoire pour le paiement TPE'
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

function calculate(url, visas, currency) {
    $.post(url, {'visas': visas, 'currency': currency}, function (data) {
        if (data.state == 'success') {
            $("input[name='v_amount']").val(data.sum);
        } else {
            swal('Error!', data.message, data.state);
        }
    });


}

function activateTpeNumber(chk, txt) {
    console.log(chk.val());
    console.log(txt.attr('placeholder'));
    if (chk.val() == "TPE") {
        txt.val("");
        txt.attr("placeholder", "Numero de transaction TPE");
        txt.attr('readonly', false);
    }
    else {
        txt.val("Paiement en espece");
        txt.attr('readonly', true);
    }
}