/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $.widget('eVisa.TableSearch', {
        options: {
            targetTable: 'table',
            filter: {
                show: true,
                data: null
            },
            sorter: {
                activate: true,
                data: null,
                default: "ASC"
            },
            pagination: {
                data: null,
                defaultPage: 1,
                maxItem: 30
            }
        },
        _create: function (options) {
            if (typeof options !== 'undifined' && options !== null) {
                $.extend({}, this.options, options);
            }
        },
        _init: function () {
            this.createFilterForm();
            this.goSearch();
        },
        goSearch: function () {
            this.loader(true);
            this.removeTable();
            this.emptyFilterForm();
            var columnFilter = '[{"key": "name", "value": ""}]';
            var columnSorter = '{"key": "id", "value": "ASC"}';
            var columnPaginate = '{"pageNumber": 1 , "maxItemByPage": 30}';


            if (this.options.filter.data != null) {
                columnFilter = this.options.filter.data;
            }

            if (this.options.sorter.data != null) {
                columnSorter = this.options.sorter.data;
            }
            if (this.options.pagination.data != null) {
                columnPaginate = this.options.pagination.data;
            }
            var app = this;

            $.ajax({
                type: "POST",
                url: app.element.attr('url-search'),
                data: {
                    filters: columnFilter,
                    sorters: columnSorter,
                    paginates: columnPaginate
                },
                success: function (data) {
                    app.loader(false);
                    $('#' + app.options.targetTable).find('tbody').append(data);
                    $('.' + app.options.loaderClass).hide();
                    $('#' + app.options.targetTable).show();
                    app.createFilterForm();
                    app.getFilter();
                    app.getSorter();
                    app.addPagination();
                }
            });
        },
        createFilterForm: function () {
            if (this.options.filter.show === true) {
                var table = $('#' + this.options.targetTable);

                var filterContainer = this.element.find('.the-filter');
                var app = this;
                $.each(table.find('th'), function () {
                    var attr = $(this).attr('type-filter');
                    if (typeof attr !== typeof undefined && attr !== false) {
                        var inputFilter = app.createInput(attr, $(this).attr('data-filter'), $(this).attr('data-placeholder'));
                        if($(this).attr('data-hide')=='true'){
                            $(inputFilter).hide();
                            $(inputFilter).val($(this).attr('default-value'));
                        }
                        var div = $('<div></div>').addClass('col-sm-6 col-xs-12 col-md-4 xs-pb-10').append(inputFilter);
                        $(filterContainer).find('form').append(div);
                    }
                });
            }
            this.element.find('.the-filter').show();
        },
        createInput: function (type, name, placeholder) {
            var app = this;
            switch (type) {
                case "select":
                    var selectList = document.createElement("select");
                    selectList.name = name;
                    if(placeholder){
                        selectList.placeholder = placeholder;
                    }
                    selectList.className += "form-control cl-name";
                    $('#foreign-' + name).find('option').clone().appendTo(selectList);
                    return selectList;
                    break;

                case "datepicker":
                    var newInput = document.createElement("input");
                    newInput.type = "text";
                    if(placeholder){
                        newInput.placeholder = placeholder;
                    }
                    newInput.name = name;
                    newInput.className += "form-control datetimepicker cl-name";
                    return newInput;
                    break;

                default:
                    var newInput = document.createElement("input");
                    if(placeholder){
                        newInput.placeholder = placeholder;
                    }
                    newInput.type = type;
                    newInput.name = name;
                    newInput.className += "form-control cl-name";
                    return newInput;
            }
        },
        getFilter: function () {
            var app = this;
            $('.button-filter').off('click').on('click', function () {
                return app.getDataFilter();
            });
            return false;
        },
        getDataFilter: function () {
            var app = this;
            var type = 'input';
            var dataTable = [];
            $('.cl-name').each(function () {
                if ($(this).is("select")) {
                    type = 'select';
                }
                console.log($(this).val());
                if ($(this).val() !== '' && $(this).val() !== "0" && $(this).val() !== null) {
                    dataTable.push({'key': $(this).attr('name'), 'value': $(this).val(), 'typeInput': type});
                }
            });
            app.options.filter.data = JSON.stringify(dataTable);
            app.goSearch();
        },
        removeTable: function () {
            $('#' + this.options.targetTable).find('tbody').empty();
            this.element.find('.pagination').remove();
        },
        emptyFilterForm: function () {
            this.element.find('.the-filter').find('form').empty();
        },
        getSorter: function () {
            var app = this;
            $('th.data-sorting').off('click').on('click', function () {
                var column = $(this).attr('data-filter');
                if (app.options.sorter.default === 'ASC') {
                    app.options.sorter.default = 'DESC';
                } else {
                    app.options.sorter.default = 'ASC';
                }
                var sorter = new Object();
                sorter.key = column;
                sorter.value = app.options.sorter.default;
                app.options.sorter.data = JSON.stringify(sorter);
                app.goSearch();
            });
            return false;
        },
        getPagination: function () {
            var app = this;
            $('ul.pagination a').off('click').on('click', function () {
                if (!$(this).attr('disabled')) {
                    app.options.pagination.defaultPage = parseInt($(this).attr('page-index'));
                    app.paginate();
                    $('ul.pagination').find('li').removeClass('active');
                    $(this).parent().addClass('active');
                }
            });
            return false;
        },
        paginate: function () {
            var pagination = new Object();
            pagination.pageNumber = this.options.pagination.defaultPage;
            pagination.maxItemByPage = this.options.pagination.maxItem;
            this.options.pagination.data = JSON.stringify(pagination);
            this.goSearch();
        },

        addPagination: function () {
            var ul = $('<ul></ul>').addClass("pagination");
            var nav = $('<nav></nav>').append(ul);
            var pagination = parseInt($('#pageCount').val());
            var currentPage = this.options.pagination.defaultPage;
            var li = document.createElement("li");
            var a = $('<a class="btn"></a>');
            var test = pagination - currentPage;
            $(a).text('précédent');
            if (test == (pagination - 1)) {
                $(a).attr('disabled', 'disabled');
            }
            $(li).append(a);
            $(nav).find('ul').append(li);
            if (pagination <= 10) {
                for (var i = 1; i <= pagination; i++) {
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text(i);
                    $(a).attr('page-index', i);
                    $(li).append(a);
                    if (i == currentPage) {
                        $(li).addClass('active');
                    }
                    $(nav).find('ul').append(li);
                }
            } else {
                if (currentPage <= 5) {
                    for (var i = 1; i <= currentPage + 2; i++) {
                        var li = document.createElement("li");
                        var a = $('<a class="btn"></a>');
                        $(a).text(i);
                        $(a).attr('page-index', i);
                        $(li).append(a);
                        if (i == currentPage) {
                            $(li).addClass('active');
                        }
                        $(nav).find('ul').append(li);
                    }
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text('... ');
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text(pagination);
                    $(a).attr('page-index', pagination);
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                } else if (test < 5) {
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text(1);
                    $(a).attr('page-index', 1);
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text('... ');
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                    for (var i = currentPage - 2; i <= pagination; i++) {
                        var li = document.createElement("li");
                        var a = $('<a class="btn"></a>');
                        $(a).text(i);
                        $(a).attr('page-index', i);
                        $(li).append(a);
                        if (i == currentPage) {
                            $(li).addClass('active');
                        }
                        $(nav).find('ul').append(li);
                    }
                } else if (test >= 5) {
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text(1);
                    $(a).attr('page-index', 1);
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text('... ');
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                    for (var i = currentPage - 2; i <= currentPage + 2; i++) {
                        var li = document.createElement("li");
                        var a = $('<a class="btn"></a>');
                        $(a).text(i);
                        $(a).attr('page-index', i);
                        $(li).append(a);
                        if (i == currentPage) {
                            $(li).addClass('active');
                        }
                        $(nav).find('ul').append(li);
                    }
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text('... ');
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                    var li = document.createElement("li");
                    var a = $('<a class="btn"></a>');
                    $(a).text(pagination);
                    $(a).attr('page-index', pagination);
                    $(li).append(a);
                    $(nav).find('ul').append(li);
                }
            }
            var li = document.createElement("li");
            var a = $('<a class="btn"></a>');
            $(a).text('suivant');
            $(li).append(a);
            if (test == 0) {
                $(a).attr('disabled', 'disabled');
            }
            $(nav).find('ul').append(li);
            $('#' + this.options.targetTable).next().find('.content-pagination').append(nav);
            this.getPagination();
        },

        loader: function (val) {
            if(val){
                var parent = this.element.find('.widget, .panel');
                $('.tablea-search-special').hide();
                if( parent.length ){
                  parent.addClass('be-loading-active');
                }
            }else{
                var parent = this.element.find('.widget, .panel');
                $('.tablea-search-special').show();
                parent.removeClass('be-loading-active');
                this.element.find('.panel-body').show();
            }
        }

    });

});