$.widget('dpaa.DataCatcher', {
	options: {
		title: '',
		filter :{
			show: false,
			list : [['email','text'],['other','datepicker'],['status','select',0]],
			commonClass: '',
			dataSelect : [],
		},
		table:{
			class: '',
			thead: [],
			tbody:[],
			pagination: {
				currentPage: 7,
				maxItem: 30,
				totalPage : 1
			},
			sorter:{
				column: '',
				order: 'ASC'
			},
			totalItem : 0
		},
		data: {
			url: 'data-url',
			sorters: '{"key": "id","value": "ASC"}',
			filters: '[]',
			paginates: '{"pageNumber": 1, "maxItemByPage": 30}'
		}

	},
	_create: function (options) {
		if (typeof options !== 'undifined' && options !== null) {
            //on surcharge les options par defaut du module par celle envoyées au constructeur
            $.extend({}, this.options, options);
        }
    },
    _init: function (){
    	this.element.find('.the-title').text(this.options.title);
    	this.loader(true);
    	this.getDatas();  
    },
    createFilterForm: function(){
    	if(this.options.filter.show === true){
    		if(typeof this.options.filter.list[0] !== 'undefined' ){
    			var arrayInput = this.options.filter.list;
    			var filterContainer = this.element.find('.the-filter');
    			for(var index in arrayInput){
    				var inputFilter = this.createInput(arrayInput[index][1],arrayInput[index][0],arrayInput[index][2]);
    				$(inputFilter).addClass(this.options.filter.commonClass);
    				var div = $('<div></div>').addClass('col-sm-6 col-xs-12 col-md-4 xs-pb-10').append(inputFilter);
    				$(filterContainer).find('form').append(div);
    			}
    		}
    		this.element.find('.the-filter').show();   		
    	}
    	
    },
    createInput :function(type,name,index){
    	var app = this;
    	switch (type){
    		case "select":
    		var selectList = document.createElement("select");
    		selectList.name = name;

    		var array = app.options.filter.dataSelect[index];
    		var defaultOption = document.createElement("option");
    		defaultOption.value = 0;
    		defaultOption.text = 'Choissisez une option';
    		selectList.appendChild(defaultOption);
    		for (var i = 0; i < array.length; i++) {
    			var option = document.createElement("option");

    			option.value = array[i].key;
    			option.text = array[i].value;
    			selectList.appendChild(option);
    		}
    		return selectList;
    		break;

    		case "datepicker":
    		var newInput = document.createElement("input");
    		newInput.type= "text";
    		newInput.name= name;
    		newInput.className += "datetimepicker";
    		return newInput;
    		break;
    		default:
    		var newInput = document.createElement("input");
    		newInput.type= type;
    		newInput.name= name;
    		return newInput;
    	}

    },
    emptyFilterForm: function(){
    	this.element.find('.the-filter').find('form').empty();
    },
    createTable: function(){
    	var table = document.createElement("table");
    	table.prepend(document.createElement('thead'));
    	table.append(document.createElement('tbody'));
    	table.className = 'the-data-table';
    	table.className += ' '+this.options.table.class;
    	this.addDataToTable(table);
    	this.element.find('.the-table').append(table);
    },
    removeTable: function(){
    	this.element.find('.table').remove();
    	this.element.find('.pagination').remove();
    },
    addDataToTable: function(table){   
    	var arrayThead = this.options.table.thead;
    	var tr = document.createElement("tr");
    	for (var i = 0; i < arrayThead.length; i++) {   
    		var th = document.createElement("th");
    		$(th).text(arrayThead[i][0]);
    		if (this.options.table.sorter.order === 'ASC' && this.options.table.sorter.column === arrayThead[i][1]) {
    			$(th).prepend('<button class="btn" style="background: none;"><i class="fa fa-sort-alpha-asc"></i> </button> ');
    		} else if(this.options.table.sorter.order === 'DESC' && this.options.table.sorter.column === arrayThead[i][1]){
    			$(th).prepend('<button class="btn" style="background: none;"><i class="fa fa-sort-alpha-desc"></i> </button> ');
    		}else{
    			$(th).prepend('<button class="btn" style="background: none;"><i class="fa fa-sort-alpha-asc"></i> </button> ');
    		}
    		$(th).attr('data-sorter',arrayThead[i][1]);
    		$(th).addClass('data-sorting');
    		$(th).css('cursor','pointer');
    		tr.appendChild(th);
    	}
    	$(table).find('thead').append(tr);

    	var arrayTbody = this.options.table.tbody;
    	$(table).find('tbody').addClass('no-border-x');
    	
    	for (index in arrayTbody){
    		var tr = document.createElement("tr");
    		for (var i = 0; i < arrayTbody[index].length; i++) {   
    			var td = document.createElement("td");
    			$(td).text(arrayTbody[index][i]);
    			tr.appendChild(td);
    		}
    		$(table).show();
    		$('.nodata-table').hide();
    		$(table).find('tbody').append(tr);
    	}
    	if($(table).find('tbody').children().length<1){
    		$(table).hide();
    		$('.nodata-table').show();
    	}
    	this.loader(false);
    },
    addPagination: function(){
    	var ul = $('<ul></ul>').addClass("pagination");
    	var nav = $('<nav></nav>').append(ul);  	
    	var pagination = this.options.table.pagination.totalPage;
    	var currentPage = this.options.table.pagination.currentPage;
    	var li = document.createElement("li");
    	var a = $('<a class="btn"></a>');
    	var test = pagination - currentPage;
    	$(a).text('précédent');
    	if(test==(pagination-1)){
    		$(a).attr('disabled','disabled');
    	}
    	$(li).append(a); 		
    	$(nav).find('ul').append(li);
    	if(pagination<=10){
    		for(var i=1; i<=pagination; i++){
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text(i);
    			$(a).attr('page-index',i);
    			$(li).append(a);
    			if(i==currentPage){
    				$(li).addClass('active');
    			}
    			$(nav).find('ul').append(li);
    		}
    	}else{
    		if(currentPage<=5){
    			for(var i=1; i<=currentPage+2; i++){
    				var li = document.createElement("li");
    				var a = $('<a class="btn"></a>');
    				$(a).text(i);
    				$(a).attr('page-index',i);
    				$(li).append(a); 
    				if(i==currentPage){
    					$(li).addClass('active');
    				}
    				$(nav).find('ul').append(li);
    			}
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text('... ');
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text(pagination);
    			$(a).attr('page-index',pagination);
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    		}else if(test<5){
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text(1);
    			$(a).attr('page-index',1);
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text('... ');
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    			for(var i=currentPage-2; i<=pagination; i++){
    				var li = document.createElement("li");
    				var a = $('<a class="btn"></a>');
    				$(a).text(i);
    				$(a).attr('page-index',i);
    				$(li).append(a); 
    				if(i==currentPage){
    					$(li).addClass('active');
    				}
    				$(nav).find('ul').append(li);
    			}
    		}else if(test>=5){
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text(1);
    			$(a).attr('page-index',1);
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text('... ');
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    			for(var i=currentPage-2; i<=currentPage+2; i++){
    				var li = document.createElement("li");
    				var a = $('<a class="btn"></a>');
    				$(a).text(i);
    				$(a).attr('page-index',i);
    				$(li).append(a); 		
    				if(i==currentPage){
    					$(li).addClass('active');
    				}
    				$(nav).find('ul').append(li);
    			}
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text('... ');
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    			var li = document.createElement("li");
    			var a = $('<a class="btn"></a>');
    			$(a).text(pagination);
    			$(a).attr('page-index',pagination);
    			$(li).append(a); 		
    			$(nav).find('ul').append(li);
    		}
    	}
    	var li = document.createElement("li");
    	var a = $('<a class="btn"></a>');
    	$(a).text('suivant');
    	$(li).append(a); 
    	if(test==0){
    		$(a).attr('disabled','disabled');
    	}
    	$(nav).find('ul').append(li);
    	var div = this.element.find('.the-pagination').append(nav);
    },
    getDatas: function(){
    	var app = this;
    	var url = app.element.attr(app.options.data.url);
    	var sorterDefault = app.options.data.sorters;
    	var filterDefault = app.options.data.filters;
    	var paginateDefault = app.options.data.paginates;

    	$.ajax({
    		type: "POST",
    		url:url,
    		data: {
    			sorters: sorterDefault,
    			filters: filterDefault,
    			paginates: paginateDefault
    		},
    		success: function(data) {
    			var dataArray = new Array();
    			$.each(data.data, function (key, val) {
    				var array = $.map(val, function (value, index) {
    					return [value];
    				});
    				dataArray.push(array);
    			});
    			app.options.filter.dataSelect = data.selectDatas;
    			
    			app.options.table.pagination.currentPage = data.pagination.pageNumber;
    			app.options.table.pagination.totalPage = data.pageCount;

    			app.options.table.totalItem = data.totalItems;

    			app.options.table.tbody = dataArray;
    			app.emptyFilterForm();
    			app.createFilterForm();
    			app.removeTable();
    			app.createTable();	
    			app.addPagination();
    			app.setTotalItem();
    			app.getPagination();
    			app.getSorter();
    			app.getFilter();
    		}
    	})
    },
    getPagination: function(){
    	var app = this; 
    	$('ul.pagination a').off('click').on('click', function () {
    		if(!$(this).attr('disabled')){
    			app.loader(true);
    			app.options.table.pagination.currentPage = parseInt($(this).attr('page-index'));
    			app.paginate();
    			app.getDatas();
    			$('ul.pagination').find('li').removeClass('active');
    			$(this).parent().addClass('active');
    		}
    	});
    	return false;
    },
    paginate: function(){
    	var pagination = new Object();
    	pagination.pageNumber = this.options.table.pagination.currentPage;
    	pagination.maxItemByPage = this.options.table.pagination.maxItem;
    	this.options.data.paginates = JSON.stringify(pagination);
    },
    getSorter: function(){
    	var app = this;
    	$('th.data-sorting').off('click').on('click', function () {
    		app.loader(true);
    		app.options.table.sorter.column = $(this).attr('data-sorter');
    		if (app.options.table.sorter.order === 'ASC') {
    			app.options.table.sorter.order = 'DESC';
    		} else {
    			app.options.table.sorter.order = 'ASC';
    		}
    		var sorter = new Object();
    		sorter.key = app.options.table.sorter.column;
    		sorter.value = app.options.table.sorter.order;
    		app.options.data.sorters = JSON.stringify(sorter);

    		app.options.table.pagination.currentPage = 1;
    		app.paginate();

    		app.getDatas();
    	});
    	return false;
    },    
    getFilter : function(){
    	var app = this;
    	$('.button-filter').off('click').on('click', function(){
    		app.loader(true);
    		return app.getDataFilter();
    	});
    	return false;
    },  
    getDataFilter : function(){
    	var app = this;
    	var type = 'input';
    	var dataTable = [];
    	$('.cl-name').each(function(){
    		if($(this).is("select")){
    			type = 'select';
    		}
    		if($(this).val()!== ''){
    			dataTable.push({'key':$(this).attr('name'), 'value':$(this).val(),'typeInput': type });
    		}		
    	});
    	app.options.data.filters = JSON.stringify(dataTable);

    	app.options.table.pagination.currentPage = 1;
    	app.paginate();
    	app.getDatas();
    },        

    loader : function(val){
    	var content = this.element.parent();
    	if(val){
    		var loader = $(document).find('.loader-dpaa').clone();
    		loader.addClass('loading');
    		loader.removeClass('loader-dpaa');
    		var height = content.height();
    		var width = content.width();
    		loader.css("position", "relative");
    		loader.css("top", height/2+"px");
    		content.find('.content-loader').append(loader);
    		content.find('.loading').show();
    		content.find('.panel-loader').height(height);
    		content.find('.panel-loader').width(width);
    		content.find('.panel-loader').show();
    	}else{
    		content.find('.panel-loader').hide();
    		content.find('.loading').remove();
    	}
    },
    setTotalItem: function(){
    	var i = this.options.table.totalItem;
    	$('.the-element-number i').text(i +' elements');
    }


});