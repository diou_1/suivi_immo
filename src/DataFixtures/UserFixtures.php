<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        //for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setFullName($faker->name);
            $user->setEmail(sprintf('admin@ifra.mg'));
            $user->setMatricule('admin');
            $user->setPassword($this->passwordEncoder->encodePassword($user,'T2021@ifra'));
            $user->setIsActive(true);
            $user->setLastUpdate(new \DateTime());
            $user->setRoles(["ROLE_ADMIN"]);
            $user->setAvatarPath("8d555cfa62c11cdcee7175c204120fb8.png");
            $manager->persist($user);
        //}
            $user = new User();
            $user->setFullName($faker->name);
            $user->setEmail(sprintf('controleur@ifra.mg'));
            $user->setMatricule('controleur');
            $user->setPassword($this->passwordEncoder->encodePassword($user,'5#UbsyX1'));
            $user->setIsActive(true);
            $user->setLastUpdate(new \DateTime());
            $user->setRoles(["ROLE_CONTROLER"]);
            $user->setAvatarPath("8d555cfa62c11cdcee7175c204120fb8.png");
            $manager->persist($user);

            $user = new User();
            $user->setFullName($faker->name);
            $user->setEmail(sprintf('info@ifra.mg'));
            $user->setMatricule('info');
            $user->setPassword($this->passwordEncoder->encodePassword($user,'KfoPeY!4'));
            $user->setIsActive(true);
            $user->setLastUpdate(new \DateTime());
            $user->setRoles(["ROLE_TECH"]);
            $user->setAvatarPath("8d555cfa62c11cdcee7175c204120fb8.png");
            $manager->persist($user);

            $user = new User();
            $user->setFullName($faker->name);
            $user->setEmail(sprintf('logistique@ifra.mg'));
            $user->setMatricule('logistique');
            $user->setPassword($this->passwordEncoder->encodePassword($user,'1iZw!dH3'));
            $user->setIsActive(true);
            $user->setLastUpdate(new \DateTime());
            $user->setRoles(["ROLE_TECH"]);
            $user->setAvatarPath("8d555cfa62c11cdcee7175c204120fb8.png");
            $manager->persist($user);

        $manager->flush();
    }
}
