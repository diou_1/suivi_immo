<?php

namespace App\DataFixtures;

use App\Entity\Agence;
use App\Entity\Antenne;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AgenceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //Agence Anosy {Anosy, Androy, Siège, Ambatobe}
        $agence = new Agence();
        $agence->setName('Anosy');
        $agence->setSurname('Tolagnaro');
        $manager->persist($agence);
                $antenne = new Antenne();
                $antenne->setName('Anosy');
                $antenne->setSurname('Tolagnaro');
                $antenne->setCodePostal('614');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Androy');
                $antenne->setSurname('Ambovombe');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Siège');
                $antenne->setSurname('Siège');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Ambatobe');
                $antenne->setSurname('Ambatobe');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

        //Agence Tuléar {Tuléar, Ampanihy}
        $agence = new Agence();
        $agence->setName('Tuléar');
        $agence->setSurname('Tuléar');
        $manager->persist($agence);
                $antenne = new Antenne();
                $antenne->setName('Tuléar');
                $antenne->setSurname('Tuléar');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Ampanihy');
                $antenne->setSurname('Ampanihy');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

        //Agence Fianarantsoa {Fianarantsoa, Ambalavao}
        $agence = new Agence();
        $agence->setName('Fianarantsoa');
        $agence->setSurname('Fianarantsoa');
        $manager->persist($agence);
                $antenne = new Antenne();
                $antenne->setName('Fianarantsoa');
                $antenne->setSurname('Fianarantsoa');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Ambalavao');
                $antenne->setSurname('Ambalavao');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

        //Agence Manakara {Manakara, Vohipeno}
        $agence = new Agence();
        $agence->setName('Manakara');
        $agence->setSurname('Manakara');
        $manager->persist($agence);
                $antenne = new Antenne();
                $antenne->setName('Manakara');
                $antenne->setSurname('Manakara');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Vohipeno');
                $antenne->setSurname('Vohipeno');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

        //Agence Ihosy {Ihosy, Betroka}
        $agence = new Agence();
        $agence->setName('Ihosy');
        $agence->setSurname('Ihosy');
        $manager->persist($agence);
                $antenne = new Antenne();
                $antenne->setName('Ihosy');
                $antenne->setSurname('Ihosy');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Betroka');
                $antenne->setSurname('Betroka');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

        //Agence Mananjary {Mananjary, Nosy Varika, Ifanadiana}
        $agence = new Agence();
        $agence->setName('Mananjary');
        $agence->setSurname('Mananjary');
        $manager->persist($agence);
                $antenne = new Antenne();
                $antenne->setName('Mananjary');
                $antenne->setSurname('Mananjary');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Nosy Varika');
                $antenne->setSurname('Nosy Varika');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Ifanadiana');
                $antenne->setSurname('Ifanadiana');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

        //Agence Farafangana {Farafangana, Vangaindrano}
        $agence = new Agence();
        $agence->setName('Farafangana');
        $agence->setSurname('Farafangana');
        $manager->persist($agence);
                $antenne = new Antenne();
                $antenne->setName('Farafangana');
                $antenne->setSurname('Farafangana');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

                $antenne = new Antenne();
                $antenne->setName('Vangaindrano');
                $antenne->setSurname('Vangaindrano');
                $antenne->setAgence($agence);
                $manager->persist($antenne);

        $manager->flush();
    }
}
