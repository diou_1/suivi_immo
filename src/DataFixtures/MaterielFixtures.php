<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MaterielFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /*$materiel = new Materiel();
        $materiel->setDesignation('Ordinateur Portable');
        $materiel->setType('Materiel');
        $materiel->setMarque('HP');
        $materiel->persist($materiel);*/

        $manager->flush();
    }
}
