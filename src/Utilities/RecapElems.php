<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utilities;

class RecapElems
{
    public $antenne;
    public $elem1;
    public $elem2;
    public $elem3;
    public $elem4;
    public $elem5;
    public $elem6;
    public $elem7;
    public $elem8;
    public $elem9;

    public function __construct($antenne, $elem1, $elem2, $elem3, $elem4, $elem5, $elem6, $elem7, $elem8, $elem9)
    {
        $this->antenne = $antenne;
        $this->elem1 = $elem1;
        $this->elem2 = $elem2;
        $this->elem3 = $elem3;
        $this->elem4 = $elem4;
        $this->elem5 = $elem5;
        $this->elem6 = $elem6;
        $this->elem7 = $elem7;
        $this->elem8 = $elem8;
        $this->elem9 = $elem9;
    }


    public function setAntenne($antenne)
    {
        $this->antenne = $antenne;
    }

    public function getAntenne()
    {
        return $this->antenne;
    }

    public function setElem1($elem1)
    {
        $this->elem1 = $elem1;
    }
    public function getElem1()
    {
        return $this->elem1;
    }

    public function setElem2($elem2)
    {
        $this->elem2 = $elem2;
    }
    public function getElem2()
    {
        return $this->elem2;
    }

    public function setElem3($elem3)
    {
        $this->elem3 = $elem3;
    }
    public function getElem3()
    {
        return $this->elem3;
    }

    public function setElem4($elem4)
    {
        $this->elem4 = $elem4;
    }
    public function getElem4()
    {
        return $this->elem4;
    }

    public function setElem5($elem5)
    {
        $this->elem1 = $elem5;
    }
    public function getElem5()
    {
        return $this->elem5;
    }

    public function setElem6($elem6)
    {
        $this->elem6 = $elem6;
    }
    public function getElem6()
    {
        return $this->elem6;
    }

    public function setElem7($elem7)
    {
        $this->elem7 = $elem7;
    }
    public function getElem7()
    {
        return $this->elem7;
    }

    public function setElem8($elem8)
    {
        $this->elem8 = $elem8;
    }
    public function getElem8()
    {
        return $this->elem8;
    }

    public function setElem9($elem9)
    {
        $this->elem9 = $elem9;
    }
    public function getElem9()
    {
        return $this->elem9;
    }
    

    public function jsonSerialize()
    {
        return array(
            "elem1" => $this->elem1,
            "elem2" => $this->elem2,
            "elem3" => $this->elem3,
            "elem4" => $this->elem4,
            "elem5" => $this->elem5,
            "elem6" => $this->elem6
        );
    }

}
