<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utilities;

class Filter
{

    public $key;
    public $value;
    public $typeInput;
    public $operator;


    public function __construct($key, $value, $typeInput = null, $operator = "OR")
    {
        $this->key = $key;
        $this->value = $value;
        $this->typeInput = $typeInput;
        $this->operator = $operator;

    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setTypeInput($typeInput)
    {
        $this->typeInput = $typeInput;
    }

    public function getTypeInput()
    {
        return $this->typeInput;
    }
    public function setOperator($operator){
        $this->operator = $operator;
    }

    public function getOperator(){
        return $this->operator;
    }


    public function jsonSerialize()
    {
        return array(
            "key" => $this->key,
            "value" => $this->value,
            "typeInput" => $this->typeInput,
            "operator" => $this->operator
        );
    }

}
