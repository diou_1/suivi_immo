<?php
/**
 * Created by PhpStorm.
 * User: Diou
 * Date: 29/09/2020
 * Time: 19:00
 */

namespace App\Utilities;

class ExportHelper
{

    public function toXlsxArray($results)
    {
		// ANTENNE | CODE IMMO | NATURE | DATE FACTURE | FOURNISSEUR | LIEU/SERVICE | RESPONSABLE | EX-RESPONSABLE | MARQUE/MODELE | CONFIGURATION | OBSERVATIONS
        $main = array();
        if (!empty($results)) {
            foreach ($results as $res) {
                $contents = [
                    $res->getAntenne()->getName(),
                    $res->getCodeImmo(),
                    $res->getNature(),
					$res->getNature(),
                    $res->getFournisseur(),
                    $res->getUtilisateur(),
                    $res->getExUtilisateur(),
                    $res->getMarque(),
					$res->getConfiguration(),
					$res->getDescription()
                ];
                array_push($main, $contents);
            }
        }
        return $main;
    }
}
