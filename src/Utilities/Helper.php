<?php

namespace App\Utilities;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;

class Helper
{

    public function denormalizeObjectArray($jsonContent, $class)
    {
        $objectArray = json_decode($jsonContent);
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $result = [];

        if (!empty($objectArray) && $objectArray != null) {
            if (is_array($objectArray)) {
                foreach ($objectArray as $key => $value) {
                    $val = json_encode($value);
                    $result[] = $serializer->deserialize($val, $class, 'json');
                }
            } else {
                $result[] = $serializer->deserialize($jsonContent, $class, 'json');
            }
        }

        return $result;
    }

    public function denormalizeObject($jsonContent, $class)
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $result = $serializer->deserialize($jsonContent, $class, 'json');
        return $result;
    }


}