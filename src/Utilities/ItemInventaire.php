<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utilities;

class ItemInventaire
{
    public $id;
    public $codeImmo;
    public $nature;
    public $dateFact;
    public $dateService;
    public $designation;
    public $centreCout;
    public $lieuService;
    public $responsable; 
    public $observation;
    public $valeurAchat;
    public $status;


    public function __construct($id, $codeImmo, $nature, $dateFact, $dateService, $centreCout, $lieuService, $responsable, $observation, $valeurAchat, $status)
    {
        $this->id = $id;
        $this->codeImmo = $codeImmo;
        $this->nature = $nature;
        $this->dateFact = $dateFact;
        $this->dateService = $dateService;
        $this->centreCout = $centreCout;
        $this->lieuService = $lieuService;
        $this->responsable = $responsable;
        $this->observation = $observation;
        $this->valeurAchat = $valeurAchat;
        $this->status = $status;
    }

    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCodeImmo()
    {
        return $this->codeImmo;
    }
    public function setCodeImmo($codeImmo)
    {
        $this->codeImmo = $codeImmo;
    }

    public function getNature()
    {
        return $this->nature;
    }
    public function setNature($nature)
    {
        $this->nature = $nature;
    }

    public function getDateFact()
    {
        return $this->dateFact;
    }
    public function setDateFact($dateFact)
    {
        $this->dateFact = $dateFact;
    }

    public function getDateService()
    {
        return $this->dateService;
    }
    public function setDateService($dateService)
    {
        $this->dateService = $dateService;
    }

    public function getDesignation()
    {
        return $this->designation;
    }
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    public function getCentreCout()
    {
        return $this->centreCout;
    }
    public function setCentreCout($centreCout)
    {
        $this->centreCout = $centreCout;
    }

    public function getLieuService()
    {
        return $this->lieuService;
    }
    public function setLieuService($lieuService)
    {
        $this->lieuService = $lieuService;
    }

    public function getResponsable()
    {
        return $this->responsable;
    }
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;
    }

    public function getObservation()
    {
        return $this->observation;
    }
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    public function getValeurAchat()
    {
        return $this->valeurAchat;
    }
    public function setValeurAchat($valeurAchat)
    {
        $this->valeurAchat = $valeurAchat;
    }

    public function getStatus()
    {
        return $this->status;
    }
    public function setStatus($status)
    {
        $this->status = $status;
    }


    public function jsonSerialize()
    {
        return array(
            "id" => $this->id
        );
    }

}
