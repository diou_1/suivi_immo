<?php

namespace App\Controller;

use App\Entity\Antenne;
use App\Entity\Materiel;
use App\Entity\Proposition;
use App\Form\MaterielType;
use App\Form\PropositionType;
use App\Repository\PropositionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

//export excel
use AppBundle\Form\Type\ExcelFormatType;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
//Spreadsheet creation
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//alignement
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class PropositionController extends AbstractController
{
    /**
     * @var PropositionRepository
     */
    private $repository;

    public function __construct(PropositionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/suivi/proposition/list", name="suivi.proposition.index")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $total = $em->getRepository(Proposition::class)->getTotal();
        $valide = $em->getRepository(Proposition::class)->getValide();
        $attente = $em->getRepository(Proposition::class)->getEnAttente();
        $refuse = $em->getRepository(Proposition::class)->getRefuse();

        $search = new Materiel();
        $mots = $request->request->get('v_mots');
        $responsable = $request->request->get('v_responsable');
        $lieuService = $request->request->get('v_lieuService');
        if($mots != null and $mots != "") {
            $search->setCodeImmo($mots);
        }
        if($responsable != null and $responsable != "") {
            $search->setResponsable($responsable);
        }
        if($lieuService != null and $lieuService != "") {
            $search->setLieuService($lieuService);
        }

        $results = $paginator->paginate(
            $this->repository->findAllVisible($search),
            $request->query->getInt('page', 1),
            10
        );
        
        return $this->render('proposition/list.html.twig', [
            'result' => $results,
            'mat' => $em->getRepository(Proposition::class)->findAllResponsable(),
            'lieu' => $em->getRepository(Proposition::class)->findAllLieuAndService(),
            'total' => $total,
            'valide' => $valide,
            'attente' => $attente,
            'refuse' => $refuse,
        ]);
    }

    /**
     * @Route("/suivi/proposition/create", name="suivi.proposition.new")
     */
    public function new(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $proposition = new Proposition();
        $form = $this->createForm(PropositionType::class, $proposition);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $proposition->setCreatedAt(new \DateTime());
            $proposition->setUsers($usr);
            $proposition->setIsValide(false);
            $proposition->setIsRefuse(false);
            $proposition->setIsVisible(true);
            $em->persist($proposition);
            $em->flush();
            $this->addFlash('add_proposition','Proposition avec succès');
            return $this->redirectToRoute('suivi.proposition.new');
        }
        return $this->render('proposition/new.html.twig', [
            'proposition' => $proposition,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/suivi/proposition/{id}", name="suivi.proposition.edit", methods="GET|POST")
     * @param Proposition $user
     * @param Request $request
     */
    public function edit(Proposition $proposition, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(PropositionType::class, $proposition);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em->merge($proposition);
            $em->flush();
            $this->addFlash('success','Modification avec succès');
            return $this->redirectToRoute('suivi.proposition.index');
        }
        return $this->render('proposition/edit.html.twig', [
            'proposition' => $proposition,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/suivi/proposition/{id}/delete", name="suivi.proposition.delete", methods="DELETE")
     * @param Proposition $proposition
     */
    public function delete(Proposition $proposition, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($this->isCsrfTokenValid('delete'.$proposition->getId(), $request->get('_token_delete'))) {
            $em->remove($proposition);
            $em->flush();
        }
        return $this->redirectToRoute('suivi.proposition.index');
    }

    /**
     * @Route("/suivi/proposition/{id}/show", name="suivi.proposition.show", methods="DELETE")
     * @param Proposition $proposition
     * @param Request $request
     */
    public function showing(Proposition $proposition, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = [];
        $data['state'] = "error";
        if($this->isCsrfTokenValid('show'.$proposition->getId(), $request->get('_token_show'))) {
            if ($proposition->getIsVisible() == true) {
                $proposition->setIsVisible(false);
                $data['message'] = "Proposition desactivé!";
                $data['state'] = "success";
            } else {
                $proposition->setIsVisible(true);
                $data['message'] = "Proposition activé!";
                $data['state'] = "success";
            }
            $em->merge($proposition);
            $em->flush();
        }
        return $this->redirectToRoute('suivi.proposition.index');
    }


    /**
     * @Route("/export/proposition", name="export_proposition")
     */
    public function exportAction(Request $request)
    {
            $option = $request->request->get('option');
            $filename = 'PROPOSITION_ACHAT_'.date( 'd-m-Y_H-i').'.xlsx';
            $em = $this->getDoctrine()->getManager();

            switch ($option) {
                case 'tous':
                    $resultall = $em->getRepository(Proposition::class)->getAll();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
            }
            $response = new StreamedResponse();
            $response->headers->set('Content-Type', $contentType);
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename.'"');
            $response->setPrivate();
            $response->headers->addCacheControlDirective('no-cache', true);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->setCallback(function() use ($writer) {
                $writer->save('php://output');
            });
            return $response;
    }


    private function createSpreadsheet($datas)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'PROPOSITIONS ACQUISITIONS DES MATERIELS')->mergeCells('A1:J1');
        $columnNames = [
            "Date",
            "Désignation",
            "Quantité",
            "Prix unitaire",
            "Centre de coût", 
            "Lieu/Service",
            "Responsable",
            "Observations",
            "status",
            "Opérateur"
        ];
        $columnLetter = 'A';
        foreach ($columnNames as $columnName) {
            $sheet->setCellValue($columnLetter++.'2', $columnName);
        }

        $columnValues = $this->toXlsxArray($datas);
        $i = 3; // Beginning row for active sheet
        foreach ($columnValues as $key => $columnValue) {
            $columnLetter = 'A';
            foreach($columnValue as $k => $v) {
                $sheet->setCellValue($columnLetter++.$i, $v);
            }
            $i++;
        }
        // Autosize each column and set style to column titles
        $columnLetter = 'A';
        foreach ($columnNames as $columnName) {
            // Center text
            $sheet->getStyle($columnLetter.'1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle($columnLetter.'2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            // Text in bold
            $sheet->getStyle($columnLetter.'1')->getFont()->setBold(true);
            $sheet->getStyle($columnLetter.'2')->getFont()->setBold(true);
            // Autosize column
            $sheet->getColumnDimension($columnLetter)->setAutoSize(true);
            $columnLetter++;
        }
        return $spreadsheet;
    }


    private function toXlsxArray($results)
    {
        $main = array();
        if (!empty($results)) {
            foreach ($results as $res) {
                $etat = null;
                if($res->getIsValide() == true and $res->getIsRefuse() == false) { $etat = 'Validé'; }
                if($res->getIsValide() == false and $res->getIsRefuse() == false) { $etat = 'En attente'; }
                if($res->getIsValide() == false and $res->getIsRefuse() == true) { $etat = 'Refusé'; }
                $contents = [
                    $res->getCreatedAt(),
                    $res->getDesignation(),
                    $res->getQte(),
                    $res->getPrixUnit(),
                    $res->getCentreCout(),
                    $res->getLieuService(),
                    $res->getResponsable(),
                    $res->getObservations(),
                    $etat,
                    $res->getUsers()->getMatricule()
                ];
                array_push($main, $contents);
            }
        }
        return $main;
    }



}
