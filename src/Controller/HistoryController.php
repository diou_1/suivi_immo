<?php

namespace App\Controller;

use App\Entity\History;
use App\Entity\Materiel;
use App\Form\MaterielType;
use App\Repository\HistoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class HistoryController extends AbstractController
{
    /**
     * @var HistoryRepository
     */
    private $repository;

    public function __construct(HistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/validation/inventaire", name="app_history_list")
     */
    public function Liste(PaginatorInterface $paginator, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $total = $em->getRepository(History::class)->getTotal();
        $valide = $em->getRepository(History::class)->isValide();
        $refuse = $em->getRepository(History::class)->isRefuse();

        $search = new Materiel();
        $form = $this->createForm(MaterielType::class, $search);
        $form->handleRequest($request);

        $results = $paginator->paginate(
            $this->repository->findAllVisible($search),
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('history/list.html.twig', [
            'result' => $results,
            'form' => $form->createView(),
            'total' => $total,
            'valide' => $valide,
            'refuse' => $refuse,
        ]);
    }

    /**
     * @Route("/switch/state/hist/{hist}", name="hist_state_switch")
     */
    public function switch($hist): Response
    {
        $em = $this->getDoctrine()->getManager();
        $hists = $em->getRepository(History::class)->find($hist);
        $data = [];
        $data['state'] = "error";
        if (null !== $hists) {
            if ($hists->getIsActive() == true) {
                $hists->setIsActive(false);
                $data['message'] = "Inventaire desactivé!";
                $data['state'] = "success";
            } else {
                $hists->setIsActive(true);
                $data['message'] = "Inventaire activé!";
                $data['state'] = "success";
            }
            $em->merge($hists);
            $em->flush();
            return $this->redirectToRoute('app_history_list');
        } else {
            $data['message'] = "Inventaire introuvable!";
        }
        return $this->render('history/list.html.twig');
    }


}
