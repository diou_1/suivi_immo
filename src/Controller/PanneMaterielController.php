<?php

namespace App\Controller;

use App\Entity\Antenne;
use App\Entity\Materiel;
use App\Entity\PanneMateriel;
use App\Entity\PropertySearch;
use App\Entity\Proposition;
use App\Form\MaterielType;
use App\Form\PanneNewType;
use App\Form\PanneType;
use App\Form\PropertySearchPanneType;
use App\Form\PropertySearchType;
use App\Repository\PanneMaterielRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

//export excel
use AppBundle\Form\Type\ExcelFormatType;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
//Spreadsheet creation
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//alignement
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class PanneMaterielController extends AbstractController
{
    /**
     * @var PanneMaterielRepository
     */
    private $repository;

    public function __construct(PanneMaterielRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @Route("/suivi/panne/create", name="suivi.panne.new")
     */
    public function new(): Response
    {
        return $this->render('panne/new.html.twig', [
            
        ]);
    }


    /**
     * @Route("/suivi/panne/search", name="suivi.panne.search")
     */
    public function search(Request $request): JsonResponse
    {
        $codeImmo = $request->request->get('v_search_code');
        $em = $this->getDoctrine()->getManager();

        $materiel = null;
        if (isset($codeImmo) && $codeImmo != "") {
            $materiel = $em->getRepository(Materiel::class)->findOneByCodeImmo($codeImmo);
        }

        $data['state'] = 'success';
        $data['html'] = $this->renderView('panne/form.html.twig', [
            'panne' => $materiel,
        ]);

        return new JsonResponse($data); 
    }


    /**
     * @Route("/suivi/panne/add", name="suivi.panne.add")
     */
    public function addPanne(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $panne = new PanneMateriel();
        if ($request->isMethod('POST')) {
            $codeImmo = $request->request->get('v_codeImmo');
            if (isset($codeImmo) && $codeImmo != "") {
                $materiel = $em->getRepository(Materiel::class)->findOneByCodeImmo($request->request->get('v_codeImmo'));
                if($materiel->getEtat() != 0){
                    $panne->setDatePanne(new \DateTime());
                    $panne->setCause($request->request->get('v_cause'));
                    $panne->setSolution($request->request->get('v_solution'));
                    $panne->setTypePanne($request->request->get('v_search_type'));
                    $panne->setDetailPanne($request->request->get('v_search_observation'));
                    $panne->setMateriel($materiel);
                    $panne->setUsers($usr);
                    $panne->setIsResolu(false);
                    $panne->setIsNonService(false);
                    //merge materiel
                    $materiel->setEtat(0);
                    $materiel->setObservation($request->request->get('v_search_observation'));
                    $em->merge($materiel);
                    //add panne
                    $em->persist($panne);
                    $em->flush();
                    $this->addFlash('add_panne','Entrée panne avec succès');
                    return $this->redirectToRoute('suivi.panne.new');
                } else {
                    $this->addFlash('exist_panne','Ce matériel ('.$codeImmo.') est déjà en panne');
                    return $this->redirectToRoute('suivi.panne.new');
                }
            } else {
                $this->addFlash('error_add_panne','Veuillez rechercher le code de matériel concerné');
            }
        }
        return $this->render('panne/new.html.twig', [
            
        ]);
    }


    /**
     * @Route("/suivi/panne/list", name="suivi.panne.index")
     */
    public function list(PaginatorInterface $paginator, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $total = $em->getRepository(PanneMateriel::class)->getTotal();
        $ko = $em->getRepository(PanneMateriel::class)->getEnPanne();
        $ok = $em->getRepository(PanneMateriel::class)->getResolu();
        $nonSrv = $em->getRepository(PanneMateriel::class)->getEnHorsService();

        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchPanneType::class, $search);
        $form->handleRequest($request);

        $results = $paginator->paginate(
            $this->repository->findAllVisible($search),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('panne/list.html.twig', [
            'result' => $results,
            'mat' => $em->getRepository(PanneMateriel::class)->findAllResponsable(),
            'lieu' => $em->getRepository(PanneMateriel::class)->findAllLieuAndService(),
            'total' => $total,
            'ko' => $ko,
            'ok' => $ok,
            'nonSrv' => $nonSrv,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/suivi/panne/{id}/delete", name="suivi.panne.delete", methods="DELETE")
     * @param PanneMateriel $panne
     */
    public function delete(PanneMateriel $panne, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($this->isCsrfTokenValid('delete'.$panne->getId(), $request->get('_token_delete'))) {
            $materiel = $panne->getMateriel();
            $materiel->setEtat(1);
            $em->merge($materiel);
            $em->remove($panne);
            $em->flush();
            //$this->addFlash('success_','Suppression avec succès');
        }
        return $this->redirectToRoute('suivi.panne.index');
    }


    /**
     * @Route("/suivi/panne/{id}/detail", name="suivi.panne.detail", methods="GET|POST")
     * @param PanneMateriel $panne
     * @param Request $request
     */
    public function detail(PanneMateriel $panne, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(PanneType::class, $panne);
        $form->handleRequest($request);
        $date = new \DateTime();
        if($form->isSubmitted() && $form->isValid()) {
            $materiel = $em->getRepository(Materiel::class)->findOneByCodeImmo($panne->getMateriel()->getCodeImmo());
            if ($request->request->get('v_etat') == "2") {
                $panne->setIsResolu(false);
                $panne->setResoluDate(null);
                $materiel->setEtat(2);
                $panne->setIsNonService(false);
            }
            if ($request->request->get('v_etat') == "1") {
                $panne->setIsResolu(true);
                $dateS = \DateTime::createFromFormat('d-m-Y', $request->request->get('v_date_reparation'));
                $dateF = new \DateTime($dateS->format("Y-m-d") . " " . $date -> format('H:i:s'));
                $panne->setResoluDate($dateF);
                $materiel->setEtat(1);
                $panne->setIsNonService(false);
            }
            if ($request->request->get('v_etat') == "4") {
                $panne->setIsResolu(false);
                $panne->setResoluDate(null);
                $materiel->setEtat(4);
                $panne->setIsNonService(true);
            }
            $em->merge($materiel);
            $em->merge($panne);
            $em->flush();
            return $this->redirectToRoute('suivi.panne.index');
        }

        return $this->render('panne/detail.html.twig', [
            'panne' => $panne,
            'date'=> $date -> format('d-m-Y'),
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/exports/panne", name="export_panne")
     */
    public function exportAction(Request $request)
    {
            $option = $request->request->get('option');
            $filename = 'MATERIELS_EN_PANNE'.date( 'd-m-Y_H-i').'.xlsx';
            $em = $this->getDoctrine()->getManager();
            $contentType = null;

            switch ($option) {
                case 'tous':
                    $resultall = $em->getRepository(PanneMateriel::class)->getAll();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
            }
            $response = new StreamedResponse();
            $response->headers->set('Content-Type', $contentType);
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename.'"');
            $response->setPrivate();
            $response->headers->addCacheControlDirective('no-cache', true);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->setCallback(function() use ($writer) {
                $writer->save('php://output');
            });
            return $response;
    }


    private function createSpreadsheet($datas)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'LISTE DES MATERIELS EN PANNE')->mergeCells('A1:I1');
        $columnNames = [
            "DATE",
            "CODE IMMO",
            "RESPONSABLE",
            "CENTRE DE COÛT", 
            "CAUSE",
            "SOLUTION",
            "ETAT",
            "CONTROLE N+1",
            "TRAITE PAR"
        ];
        $columnLetter = 'A';
        foreach ($columnNames as $columnName) {
            $sheet->setCellValue($columnLetter++.'2', $columnName);
        }

        $columnValues = $this->toXlsxArray($datas);
        $i = 3; // Beginning row for active sheet
        foreach ($columnValues as $key => $columnValue) {
            $columnLetter = 'A';
            foreach($columnValue as $k => $v) {
                $sheet->setCellValue($columnLetter++.$i, $v);
            }
            $i++;
        }
        // Autosize each column and set style to column titles
        $columnLetter = 'A';
        foreach ($columnNames as $columnName) {
            // Center text
            $sheet->getStyle($columnLetter.'1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle($columnLetter.'2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            // Text in bold
            $sheet->getStyle($columnLetter.'1')->getFont()->setBold(true);
            $sheet->getStyle($columnLetter.'2')->getFont()->setBold(true);
            // Autosize column
            $sheet->getColumnDimension($columnLetter)->setAutoSize(true);
            $columnLetter++;
        }
        return $spreadsheet;
    }

    private function toXlsxArray($results)
    {
        $main = array();
        if (!empty($results)) {
            foreach ($results as $res) {
                $etat = null;
                if($res->getIsResolu() == true) { $etat = 'Resolu'; }
                if($res->getIsNonService() == true) { $etat = 'Hors service'; }
                if($res->getIsResolu() == false and $res->getIsNonService() == false) { $etat = 'En panne'; }
                $contents = [
                    $res->getDatePanne(),
                    $res->getMateriel()->getCodeImmo(),
                    $res->getMateriel()->getResponsable(),
                    $res->getMateriel()->getLieuService(),
                    $res->getCause(),
                    $res->getSolution(),
                    $etat,
                    $res->getCommentn1(),
                    $res->getUsers()->getMatricule()
                ];
                array_push($main, $contents);
            }
        }
        return $main;
    }


}
