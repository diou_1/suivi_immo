<?php

namespace App\Controller;

use App\Entity\Agence;
use App\Entity\Antenne;
use App\Entity\Materiel;
use App\Entity\Proposition;
use App\Utilities\Filter;
use App\Utilities\Helper;
use App\Utilities\RecapElems;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(AuthorizationCheckerInterface $authChecker)
    {
        $res = null;
        if ($authChecker->isGranted('ROLE_ADMIN') 
            or $authChecker->isGranted('ROLE_CONTROLER')
            or $authChecker->isGranted('ROLE_TECH')
            or $authChecker->isGranted('ROLE_LOG'))
        {
            $res = $this->redirectToRoute('suivi.recap.index');
        } else {
            $res = $this->redirectToRoute('app_login');
        }
        return $res;
    }

    /**
    * @Route("/dashboard", name="suivi.recap.index")
    */
    public function recap(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $BE = $em->getRepository(Materiel::class)->getBE();
        $Moyen = $em->getRepository(Materiel::class)->getMoyen();
        $Mauvais = $em->getRepository(Materiel::class)->getMauvais();
        $Panne = $em->getRepository(Materiel::class)->getPanne();
        $HorsService = $em->getRepository(Materiel::class)->getHorsService();
        $perdu = $em->getRepository(Materiel::class)->getPerdu();

        
        $valide = $em->getRepository(Proposition::class)->getValide();
        $attente = $em->getRepository(Proposition::class)->getEnAttente();
        $refuse = $em->getRepository(Proposition::class)->getRefuse();

        // 05 Prémière Propositions
        $Propositions_5 = $em->getRepository(Proposition::class)->getFiveFirstPropositions();

        return $this->render('default/recap.html.twig', [
            'propositions' => $Propositions_5,
            'BE' => $BE,
            'Moyen' => $Moyen,
            'Mauvais' => $Mauvais,
            'Panne' => $Panne,
            'HorsService' => $HorsService,
            'Valide' => $valide,
            'Attente' => $attente,
            'Refuse' => $refuse,
            'Perdu' => $perdu,
        ]);
    }

    /**
     * @Route("/suivi/proposition/{id}/valide", name="suivi.proposition.valide", methods="DELETE")
     * @param Proposition $proposition
     * @param Request $request
     */
    public function valide(Proposition $proposition, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = [];
        $data['state'] = "error";
        if($this->isCsrfTokenValid('valide'.$proposition->getId(), $request->get('_token_valide'))) {
            $proposition->setIsValide(true);
            $proposition->setControlAt(new \DateTime());
            $proposition->setIsRefuse(false);
            $data['message'] = "Proposition validé!";
            $data['state'] = "success";
            $em->merge($proposition);
            $em->flush();
        }
        return $this->redirectToRoute('suivi.recap.index');
    }

    /**
     * @Route("/suivi/proposition/{id}/attente", name="suivi.proposition.attente", methods="DELETE")
     * @param Proposition $proposition
     * @param Request $request
     */
    public function attente(Proposition $proposition, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = [];
        $data['state'] = "error";
        if($this->isCsrfTokenValid('attente'.$proposition->getId(), $request->get('_token_attente'))) {
            $proposition->setIsValide(false);
            $proposition->setControlAt(new \DateTime());
            $proposition->setIsRefuse(false);
            $data['message'] = "Proposition en attente!";
            $data['state'] = "success";
            $em->merge($proposition);
            $em->flush();
        }
        return $this->redirectToRoute('suivi.recap.index');
    }


    /**
     * @Route("/suivi/proposition/{id}/refuse", name="suivi.proposition.refuse", methods="DELETE")
     * @param Proposition $proposition
     * @param Request $request
     */
    public function refuse(Proposition $proposition, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = [];
        $data['state'] = "error";
        if($this->isCsrfTokenValid('refuse'.$proposition->getId(), $request->get('_token_refuse'))) {
            $proposition->setIsRefuse(true);
            $proposition->setControlAt(new \DateTime());
            $proposition->setIsValide(false);
            $data['message'] = "Proposition refusé!";
            $data['state'] = "success";
            $em->merge($proposition);
            $em->flush();
        }
        return $this->redirectToRoute('suivi.recap.index');
    }



}
