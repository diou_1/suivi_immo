<?php

namespace App\Controller;

use App\Entity\Materiel;
use App\Entity\User;
use App\Form\UserEditType;
use App\Form\UserResetType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class SecurityController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $repository;

    private $passwordEncoder;

    public function __construct(UserRepository $repository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->repository = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }


    /**
    * @Route("/login", name="app_login")
    */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            $this->redirectToRoute('suivi.recap.index');
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername, 
            'error' => $error
        ]);
    }


    /**
     * @Route("/admin/users/create", name="app_register")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $usr = $em->getRepository(User::class)->loadUserByUsername($user->getMatricule());
            if (null == $usr) {
                $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
                $user->setEmail($user->getMatricule() . '@ifra.mg');
                $file = $user->getAvatar();
                if (is_file($file)) {
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('upload_directory_img'), $fileName);
                    $user->setAvatarPath($fileName);
                } else {
                    $user->setAvatarPath('8d555cfa62c11cdcee7175c204120fb8.jpg');
                }
                $arrayRoles = [];
                if ($request->request->get('logistic') == "1") {
                    array_push($arrayRoles, "ROLE_LOG");
                }
                if ($request->request->get('technic') == "1") {
                    array_push($arrayRoles, "ROLE_TECH");
                }
                if ($request->request->get('controler') == "1") {
                    array_push($arrayRoles, "ROLE_CONTROLER");
                }
                if ($request->request->get('consultation') == "1") {
                    array_push($arrayRoles, "ROLE_USER");
                }
                $user->setRoles($arrayRoles);
                
                $user->setIsActive(true);
                $user->setLastUpdate(new \DateTime());
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('app_users');
            } else {
                $this->addFlash('success_user', 'Utilisateur '.$user->getMatricule().' est déjà existe');
            }
        }
        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/admin/users/{id}", name="app_user_updated")
     * @param User $user
     * @param Request $request
     */
    public function edit(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
                $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
                $user->setEmail($user->getMatricule() . '@ifra.mg');
                $file = $user->getAvatar();
                if (is_file($file)) {
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('upload_directory_img'), $fileName);
                    $user->setAvatarPath($fileName);
                }
                $arrayRoles = [];
                if ($request->request->get('logistic') == "1") {
                    array_push($arrayRoles, "ROLE_LOG");
                }
                if ($request->request->get('technic') == "1") {
                    array_push($arrayRoles, "ROLE_TECH");
                }
                if ($request->request->get('controler') == "1") {
                    array_push($arrayRoles, "ROLE_CONTROLER");
                }
                if ($request->request->get('consultation') == "1") {
                    array_push($arrayRoles, "ROLE_USER");
                }
                $user->setRoles($arrayRoles);
                $user->setIsActive(true);
                $user->setLastUpdate(new \DateTime());
                $em->merge($user);
                $em->flush();
                $this->addFlash('success_user', 'Modification avec succès');
                return $this->redirectToRoute('app_users');
        }

        return $this->render('security/update.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/admin/users", name="app_users")
     */
    public function list(PaginatorInterface $paginator, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $total = $em->getRepository(User::class)->getTotal();

        $search = new Materiel();
        $mots = $request->request->get('v_mots');
        if($mots != null and $mots != "") {
            $search->setCodeImmo($mots);
        }

        $results = $paginator->paginate(
            $this->repository->findAllVisible($search),
            $request->query->getInt('page', 1),
            8
        );
        
        return $this->render('security/list.html.twig', [
            'result' => $results,
            'users' => $em->getRepository(User::class)->findAllMatricule(),
            'total' => $total,
        ]); 
    }

    /**
     * @Route("/admin/users/{id}/reset", name="admin.users.reset", methods="DELETE")
     * @param User $user
     * @param Request $request
     */
    public function reset(User $user, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($this->isCsrfTokenValid('reset'.$user->getId(), $request->get('_token_reset'))) {
            $user->setPassword('$argon2i$v=19$m=65536,t=4,p=1$NEpPL0lPNWZ2RW1aYlpmWA$pJIrT6+oaFNvC9nuwu+AYjva6cDi0rcywwrCIJVSlfU');
            $em->merge($user);
            $em->flush();
            $this->addFlash('success_user', 'Mot de passe d\'utilisateur '.$user->getMatricule().' a été reinitialisé');
        }
        return $this->redirectToRoute('app_users');
    }

    /**
     * @Route("/admin/users/{id}/switch", name="admin.users.switch", methods="DELETE")
     * @param User $user
     */
    public function switch(User $user, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->find($user);
        $data = [];
        $data['state'] = "error";
        if($this->isCsrfTokenValid('switch'.$user->getId(), $request->get('_token_switch'))) {
            if ($users->getIsActive() == true) {
                $users->setIsActive(false);
                $data['message'] = "Utilisateur desactivé!";
                $data['state'] = "success";
            } else {
                $users->setIsActive(true);
                $data['message'] = "Utilisateur activé!";
                $data['state'] = "success";
            }
            $em->merge($users);
            $em->flush();
        }
        return $this->redirectToRoute('app_users');
    }


    /**
     * @Route("/password/users/{id}/change", name="admin.users.change", methods="GET|POST")
     * @param User $user
     * @param Request $request
     */
    public function ChangePassword(User $user, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(UserResetType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($this->passwordEncoder->encodePassword($user,$user->getPassword()));
            $em->merge($user);
            $em->flush();
            //$this->addFlash('success','Utilisateur modifié avec succès');
            return $this->redirectToRoute('app_logout');
        }

        return $this->render('profile/change.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }


    /**
    * @Route("/logout", name="app_logout")
    */
    public function logout()
    {
        return $this->redirectToRoute('login');
    }


}
