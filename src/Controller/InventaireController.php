<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Upload;
use App\Form\UploadType;
use App\Entity\Materiel;
use App\Entity\Antenne;
use App\Entity\History;
use App\Entity\PanneMateriel;
use App\Entity\PropertySearch;
use App\Form\MaterielType;
use App\Form\PropertySearchType;
use App\Repository\MaterielRepository;
use App\Utilities\ExportHelper;
use App\Utilities\Filter;
use App\Utilities\Helper;
use App\Utilities\ItemInventaire;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

use Phppot\DataSource;
//use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

//export excel
use AppBundle\Form\Type\ExcelFormatType;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
//Spreadsheet creation
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//alignement
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class InventaireController extends AbstractController
{

    /**
     * @var MaterielRepository
     */
    private $repository;

    public function __construct(MaterielRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/materiels/import", name="app_inventaire_import")
     */
    public function import(Request $request): Response
    {
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $date = new \DateTime();

        $helper = new Helper(); 
        $result = null;
        $Object = '[]';
        $ObjectArray = $helper->denormalizeObjectArray($Object, Filter::class);

        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid()){
            $file = $upload->getName();
            $filename = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory_inventaire'), $filename);
            $upload->setName($filename);
            $targetPath = $this->getParameter('upload_directory_inventaire').$filename;
            //Methode 1
            $spreadSheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($targetPath);
            $data = $spreadSheet->getActiveSheet()->toArray();

            $hist = new History();
            $hist->setCreatedAt(new \DateTime());
            $hist->setIsActive(true);
            $hist->setUsers($usr);
            $hist->setFilepath($filename);
            $hist->setType("Inventaire");
            //$em->persist($hist);
            //$status = $hist;

            $i = 0;
            $qte = 0;
            $valide = false;
            $exist = false;
            $status = null;
                foreach($data as $row) {
                    if($i == 0){
                        if($row[0] == "Code Immo corpo" and $row[1] == "Nature" and $row[2] == "Date facture" and $row[3] == "Date de mise en service" 
                        and $row[4] == "Désignation" and $row[5] == "Centre de coût" and $row[6] == "Lieu/Service" and $row[7] == "Responsable" 
                        and $row[8] == "Observations" and $row[9] == "Valeur d'achat" and $row[10] == "Etat") {
                            $valide = true;
                        }
                    } else {
                        if($valide == true) {
                            if($row[0] != null and $row[0] != ""){
                                $materiel = $em->getRepository(Materiel::class)->findOneByCodeImmo($row[0]);
                                if ($materiel != null) {
                                    $exist = true;
                                    $status = 'exist';
                                } else {
                                    $exist = false;
                                    $status = null;
                                }
                            } else {
                                $exist = false;
                                $status = null;
                            }
                            if($exist == false and $status == null){
                                $materiel = new Materiel();
                                $codeImmo = $row[0];
                                $nature = $row[1];
                                $designation = str_replace("\"", "", $row[4]);
                                $centre_cout = $row[5];
                                $lieu_service = $row[6];
                                $responsable = $row[7]; 
                                $observations = $row[8];
                                $valeurAchat = str_replace(",", "", $row[9]);

                                //convert date
                                $dateFacture = null;
                                if($row[2] != null and $row[2] != ""){
                                    $fromDate = \DateTime::createFromFormat('m/d/Y',$row[2]);
                                    $dateToString = $fromDate->format("d/m/Y");
                                    $ToDate = new \DateTime($fromDate->format("Y-m-d"));
                                    $dateFacture = $ToDate;
                                }

                                $dateService = null;
                                if($row[3] != null and $row[3] != ""){
                                    $fromDate = \DateTime::createFromFormat('m/d/Y',$row[3]);
                                    $dateToString = $fromDate->format("d/m/Y");
                                    $ToDate = new \DateTime($fromDate->format("Y-m-d"));
                                    $dateService = $ToDate;
                                }
                                
                                if($row[0] != null and $row[0] != ""){
                                    $materiel->setCodeImmo($codeImmo);
                                }
                                if($row[1] != null and $row[1] != ""){
                                    $materiel->setNature($nature);
                                }
                                if($row[2] != null and $row[2] != ""){
                                    $materiel->setDateFacture($dateFacture);
                                }
                                if($row[3] != null and $row[3] != ""){
                                    $materiel->setDateService($dateService);
                                }
                                if($row[4] != null and $row[4] != ""){
                                    $materiel->setDesignation($designation);
                                }
                                if($row[5] != null and $row[5] != ""){
                                    $materiel->setCentreCout($centre_cout);
                                }
                                if($row[6] != null and $row[6] != ""){
                                    $materiel->setLieuService($lieu_service);
                                }
                                if($row[7] != null and $row[7] != ""){
                                    $materiel->setResponsable($responsable);
                                }

                                if($row[8] != null and $row[8] != ""){
                                    $materiel->setObservation($observations);
                                }

                                if($row[9] != null and $row[9] != ""){
                                    $materiel->setValeurAchat($valeurAchat);
                                }

                                if($row[10] != null and $row[10] != ""){
                                    if($row[10] == 1){
                                        $materiel->setEtat(1);
                                    }
                                    if($row[10] == 2){
                                        $materiel->setEtat(2);
                                    }
                                    if($row[10] == 3){
                                        $materiel->setEtat(3);
                                    }
                                    if($row[10] == 4){
                                        $panne = new PanneMateriel();
                                        $date1 = \DateTime::createFromFormat('d-m-Y', '01-01-2020');
                                        $date2 = new \DateTime($date1->format("Y-m-d") . " " . $date -> format('H:i:s'));
                                        $panne->setDatePanne($date2);
                                        $panne->setCause($observations);
                                        $panne->setSolution("");
                                        $panne->setTypePanne("Matériel");
                                        $panne->setDetailPanne($observations);
                                        $panne->setMateriel($materiel);
                                        $panne->setUsers($usr);
                                        $panne->setIsResolu(false);
                                        $panne->setIsNonService(false);
                                        $materiel->setEtat(4);
                                        $em->persist($panne);
                                    }
                                    if($row[10] == 5){
                                        $panne = new PanneMateriel();
                                        $date1 = \DateTime::createFromFormat('d-m-Y', '01-01-2020');
                                        $date2 = new \DateTime($date1->format("Y-m-d") . " " . $date -> format('H:i:s'));
                                        $panne->setDatePanne($date2);
                                        $panne->setCause($observations);
                                        $panne->setSolution("");
                                        $panne->setTypePanne("Matériel");
                                        $panne->setDetailPanne($observations);
                                        $panne->setMateriel($materiel);
                                        $panne->setUsers($usr);
                                        $panne->setIsResolu(false);
                                        $panne->setIsNonService(true);
                                        $materiel->setEtat(5);
                                        $em->persist($panne);
                                    }
                                    if($row[10] == 6){
                                        $materiel->setEtat(6);
                                    }
                                } else {
                                    $materiel->setEtat(0);
                                }

                                if($hist != null) {
                                    $materiel->setHistory($hist);
                                    $materiel->setUsers($usr);
                                }

                                $em->persist($materiel);
                                $qte = $qte + 1;
                            }

                            //convert date
                            $dateFactureToString = null;
                            if($row[2] != null and $row[2] != ""){
                                $fromDate = \DateTime::createFromFormat('m/d/Y',$row[2]);
                                $dateFactureToString = $fromDate->format("d/m/Y");
                            }
                            $dateServiceToString = null;
                            if($row[3] != null and $row[3] != ""){
                                $fromDate = \DateTime::createFromFormat('m/d/Y',$row[3]);
                                $dateServiceToString = $fromDate->format("d/m/Y");
                            }

                            //$id, $codeImmo, $nature, $dateFact, $dateService, $designation, $centreCout, $lieuService, $responsable, $observation, $valeurAchat, $status
                            $ligne = '{"id":"'.$i.'", "codeImmo": "' . $row[0] . '","nature":"'.$row[1].'","dateFact":"'.$dateFactureToString.'", "dateService":"'.$dateServiceToString.'", "designation":"'.$row[4].'", "centreCout":"'.$row[5].'", "lieuService":"'.$row[6].'", "responsable":"'.$row[7].'", "observation":"'.$row[8].'", "valeurAchat":"'.$valeurAchat.'", "status":"'.$status.'"}';
                            $Object = $helper->denormalizeObject($ligne, ItemInventaire::class);
                            array_push($ObjectArray, $Object);
                        }
                    }
                    $i++;
                }

                if($qte>0){
                    $hist->setQuantite($qte);
                    $em->persist($hist);
                    $result = $ObjectArray;
                    $em->flush();
                    $this->addFlash('success', 'Avec succès');
                } else {
                    $this->addFlash('success', 'Erreur');
                }
        }

        return $this->render('immo/index.html.twig', [
            'form' => $form->createView(),
            'result' => $result,
        ]);
    }


    /**
     * @Route("/suivi/immo/create", name="suivi.immo.new")
     */
    public function new(Request $request)
    {
        $materiel = new Materiel();
        $em = $this->getDoctrine()->getManager();
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(MaterielType::class, $materiel);
        $form->handleRequest($request);
        $date = new \DateTime();

        if ($form->isSubmitted() && $form->isValid()) {
            $mat = $em->getRepository(Materiel::class)->findOneByCodeImmo($materiel->getCodeImmo());
            if ($mat == null) {
                $dateFacture = $request->request->get('v_dateFacture');
                $dateService = $request->request->get('v_dateService');

                $dateF1 = \DateTime::createFromFormat('d-m-Y', $dateFacture);
                $dateF2 = new \DateTime($dateF1->format("Y-m-d") . " " . $date -> format('H:i:s'));
                $materiel->setDateFacture($dateF2);

                if($dateService != null and $dateService != "") {
                    $dateS1 = \DateTime::createFromFormat('d-m-Y', $dateService);
                    $dateS2 = new \DateTime($dateS1->format("Y-m-d") . " " . $date -> format('H:i:s'));
                    $materiel->setDateService($dateS2);
                }
                $materiel->setEtat(1);
                $materiel->setUsers($usr);
                $em->persist($materiel);
                $em->flush();
                $this->addFlash('add_materiel','Ajout matériel avec succès');
                return $this->redirectToRoute('suivi.immo.new');
            }
            else {
                $this->addFlash('exist_materiel','Code immo '.$materiel->getCodeImmo().' est déjà existe');
            }
        }

        return $this->render('immo/new.html.twig', [
            'materiel' => $materiel,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/suivi/immo/{id}/detail", name="suivi.immo.detail", methods="GET|POST")
     * @param Materiel $materiel
     * @param Request $request
     */
    public function detail(Materiel $materiel, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(MaterielType::class, $materiel);
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $form->handleRequest($request);
        $date = new \DateTime();

        if($form->isSubmitted() && $form->isValid()) {
            $dateFacture = $request->request->get('v_dateFacture');
            $dateService = $request->request->get('v_dateService');
            if($dateFacture != null and $dateFacture != "") {
                $dateF1 = \DateTime::createFromFormat('d-m-Y', $dateFacture);
                $dateF2 = new \DateTime($dateF1->format("Y-m-d") . " " . $date -> format('H:i:s'));
                $materiel->setDateFacture($dateF2);
            }
            if($dateService != null and $dateService != "") {
                $dateS1 = \DateTime::createFromFormat('d-m-Y', $dateService);
                $dateS2 = new \DateTime($dateS1->format("Y-m-d") . " " . $date -> format('H:i:s'));
                $materiel->setDateService($dateS2);
            }
            if ($request->request->get('v_etat') == "1") {
                $materiel->setEtat(1);
            }
            if ($request->request->get('v_etat') == "2") {
                $materiel->setEtat(2);
            }
            if ($request->request->get('v_etat') == "3") {
                $materiel->setEtat(3);
            }
            if ($request->request->get('v_etat') == "6") {
                $materiel->setEtat(6);
            }
            if ($request->request->get('v_etat') == "0") {
                $materiel->setEtat(0);
            }
            $materiel->setUserEdit($usr);
            $materiel->setEditDate(new \DateTime());
            $em->merge($materiel);
            $em->flush();
            return $this->redirectToRoute('suivi.immo.index');
        }
        return $this->render('immo/detail.html.twig', [
            'date'=> $date -> format('d-m-Y'),
            'materiel' => $materiel,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/suivi/immo/list", name="suivi.immo.index")
     */
    public function list(PaginatorInterface $paginator, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $total = $em->getRepository(Materiel::class)->getTotal();
        $BE = $em->getRepository(Materiel::class)->getBE();
        $Moyen = $em->getRepository(Materiel::class)->getMoyen();
        $Mauvais = $em->getRepository(Materiel::class)->getMauvais();
        $Panne = $em->getRepository(Materiel::class)->getPanne();
        $HorsService = $em->getRepository(Materiel::class)->getHorsService(); 
        $perdu = $em->getRepository(Materiel::class)->getPerdu();
        $stock = $em->getRepository(Materiel::class)->getStock();

        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        $results = $paginator->paginate(
            $this->repository->findAllVisible($search),
            $request->query->getInt('page', 1),
            10
        ); 

        return $this->render('immo/list.html.twig', [
            'result' => $results,
            'mat' => $em->getRepository(Materiel::class)->findAllResponsable(),
            'total' => $total,
            'BE' => $BE,
            'Moyen' => $Moyen,
            'Mauvais' => $Mauvais,
            'Panne' => $Panne,
            'HorsService' => $HorsService,
            'Perdu' => $perdu,
            'Stock' => $stock,
            'form' => $form->createView()
        ]); 
    }


    /**
     * @Route("/suivi/immo/{id}/delete", name="suivi.immo.delete", methods="DELETE")
     * @param Materiel $materiel
     */
    public function delete(Materiel $materiel, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($this->isCsrfTokenValid('delete'.$materiel->getId(), $request->get('_token_delete'))) {
            $panne = $em->getRepository(PanneMateriel::class)->findOneByIdMateriel($materiel->getId());
            if($panne != null) {
                $em->remove($panne);
            }
            $em->remove($materiel);
            $em->flush();
            $this->addFlash('success_','Suppression avec succès');
        }
        return $this->redirectToRoute('suivi.immo.index');
    }


    /**
     * @Route("/exports", name="export")
     */
    public function exportAction(Request $request)
    {
            $option = $request->request->get('option');
            $filename = 'SUIVI_IMMO_'.date( 'd-m-Y_H-i').'.xlsx';
            $em = $this->getDoctrine()->getManager();
            $contentType = null;

            switch ($option) {
                case 'tous':
                    $filename = 'SUIVI_IMMO_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getAll();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
                case '1':
                    $filename = 'MATERIELS_BON_ETAT_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getListBE();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
                case '2':
                    $filename = 'MATERIELS_MOYEN_ETAT_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getListMoyen();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
                case '3':
                    $filename = 'MATERIELS_MAUVAIS_ETAT_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getListMauvais();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
                case '4':
                    $filename = 'MATERIELS_PANNE_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getListPanne();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
                case '5':
                    $filename = 'MATERIELS_HORS_SERVICE_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getListHorsService();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
                case '6':
                    $filename = 'MATERIELS_PERDU_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getListPerdu();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
                case '0':
                    $filename = 'MATERIELS_EN_STOCK_'.date( 'd-m-Y_H-i').'.xlsx';
                    $resultall = $em->getRepository(Materiel::class)->getListEnStock();
                    $spreadsheet = $this->createSpreadsheet($resultall);
                    $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    $writer = new Xlsx($spreadsheet);
                    break;
            }
            $response = new StreamedResponse();
            $response->headers->set('Content-Type', $contentType);
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename.'"');
            $response->setPrivate();
            $response->headers->addCacheControlDirective('no-cache', true);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->setCallback(function() use ($writer) {
                $writer->save('php://output');
            });
            return $response;
    }


    private function createSpreadsheet($datas)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'SUIVI IMMOBILISATIONS')->mergeCells('A1:K1');
        // ANTENNE | CODE IMMO | NATURE | DATE FACTURE | FOURNISSEUR | LIEU/SERVICE | RESPONSABLE | EX-RESPONSABLE | MARQUE/MODELE | CONFIGURATION | OBSERVATIONS
        $columnNames = [
            "Code Immo corpo",
            "Nature",
            "Date facture",
            "Date de mise en service", 
            "Désignation",
            "Centre de coût",
            "Lieu/Service",
            "Responsable",
            "Observations",
            "Valeur d'achat",
            "Etat"
        ];
        $columnLetter = 'A';
        foreach ($columnNames as $columnName) {
            $sheet->setCellValue($columnLetter++.'2', $columnName);
        }

        $columnValues = $this->toXlsxArray($datas);
        $i = 3; // Beginning row for active sheet
        foreach ($columnValues as $key => $columnValue) {
            $columnLetter = 'A';
            foreach($columnValue as $k => $v) {
                $sheet->setCellValue($columnLetter++.$i, $v);
            }
            $i++;
        }

        // Autosize each column and set style to column titles
        $columnLetter = 'A';
        foreach ($columnNames as $columnName) {
            // Center text
            $sheet->getStyle($columnLetter.'1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle($columnLetter.'2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            // Text in bold
            $sheet->getStyle($columnLetter.'1')->getFont()->setBold(true);
            $sheet->getStyle($columnLetter.'2')->getFont()->setBold(true);
            // Autosize column
            $sheet->getColumnDimension($columnLetter)->setAutoSize(true);
            $columnLetter++;
        }
        return $spreadsheet;
    }


    private function toXlsxArray($results)
    {
		// ANTENNE | CODE IMMO | NATURE | DATE FACTURE | FOURNISSEUR | LIEU/SERVICE | RESPONSABLE | EX-RESPONSABLE | MARQUE/MODELE | CONFIGURATION | OBSERVATIONS
        $main = array();
        if (!empty($results)) {
            foreach ($results as $res) {
                $etat = null;
                if($res->getEtat() == 1) { $etat = 'Bon état'; }
                if($res->getEtat() == 2) { $etat = 'Moyen'; }
                if($res->getEtat() == 3) { $etat = 'Mauvais'; }
                if($res->getEtat() == 4) { $etat = 'En panne'; }
                if($res->getEtat() == 5) { $etat = 'Hors service'; }
                if($res->getEtat() == 6) { $etat = 'Perdu'; }
                if($res->getEtat() == 0) { $etat = 'En stock'; }

                $contents = [
                    $res->getCodeImmo(),
                    $res->getNature(),
					$res->getDateFacture(),
                    $res->getDateService(),
                    $res->getDesignation(),
					$res->getCentreCout(),
                    $res->getLieuService(),
                    $res->getResponsable(),
                    $res->getObservation(),
                    $res->getValeurAchat(),
                    $etat
                ];
                array_push($main, $contents);
            }
        }
        return $main;
    }



}
