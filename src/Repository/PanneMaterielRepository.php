<?php

namespace App\Repository;

use App\Entity\PanneMateriel;
use App\Entity\Materiel;
use App\Entity\PropertySearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @method PanneMateriel|null find($id, $lockMode = null, $lockVersion = null)
 * @method PanneMateriel|null findOneBy(array $criteria, array $orderBy = null)
 * @method PanneMateriel[]    findAll()
 * @method PanneMateriel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PanneMaterielRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PanneMateriel::class);
    }

    // /**
    //  * @return PanneMateriel[] Returns an array of PanneMateriel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PanneMateriel
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return Query
     */
    public function findAllVisible(PropertySearch $search): Query
    {
        $query = $this->createQueryBuilder('p');
        $query->join('p.materiel','mat');
        //$query->join('mat.history','hist');
        //$query->where('hist.isActive = true');
        if ($search->getCode()) {
            $query->andWhere('mat.codeImmo like :mots');
            $query->setParameter('mots', '%'.$search->getCode().'%');
        }
        if ($search->getResponsable()) {
            $query->andWhere('mat.responsable like :resp');
            $query->setParameter('resp', '%'.$search->getResponsable().'%');
        }
        if ($search->getLieuService()) {
            $query->andWhere('mat.lieuService like :lieuS');
            $query->setParameter('lieuS', '%'.$search->getLieuService().'%');
        }
        return $query->getQuery();
    }

    public function findAllResponsable()
    {
        return $this->createQueryBuilder('p')
            ->join('p.materiel','mat')
            ->select('mat.responsable as responsable')
            ->orderBy('mat.responsable','ASC')
            ->groupBy('mat.responsable')
            ->getQuery()
            ->getResult();
    }

    public function findAllLieuAndService()
    {
        return $this->createQueryBuilder('p')
            ->join('p.materiel','mat')
            ->select('mat.lieuService as lieuService')
            ->orderBy('mat.lieuService','ASC')
            ->groupBy('mat.lieuService')
            ->getQuery()
            ->getResult();
    }

    public function getAll()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->join('p.materiel','mat');
        //$this->queryBuilder->join('mat.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getTotal()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->join('p.materiel','mat');
        //$this->queryBuilder->join('mat.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $total = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $total[0]['nb'];
    }

    public function getEnPanne()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->join('p.materiel','mat');
        //$this->queryBuilder->join('mat.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.isResolu = false and p.isNonService = false');
        $panne = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $panne[0]['nb'];
    }

    public function getResolu()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->join('p.materiel','mat');
        //$this->queryBuilder->join('mat.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.isResolu = true');
        $resolu = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $resolu[0]['nb'];
    }

    public function getEnHorsService()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->join('p.materiel','mat');
        //$this->queryBuilder->join('mat.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.isNonService = true');
        $nonService = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $nonService[0]['nb'];
    }

    public function findOneByIdMateriel($value): ?PanneMateriel
    {
        return $this->createQueryBuilder('p')
            ->join('p.materiel','mat')
            ->andWhere('mat.id like :id')
            ->setParameter('id', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
