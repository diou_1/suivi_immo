<?php

namespace App\Repository;

use App\Entity\Materiel;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
    */

    /*public function getAllUsers($value, $dates)
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        if ($value != null){
            $this->queryBuilder->andWhere('p.fullName like :val or p.matricule like :val or p.email like :val');
            $this->queryBuilder->setParameter('val', '%'.$value.'%');
        }
        if ($dates != null) {
            $a = \DateTime::createFromFormat('d-m-Y', $dates);
            $b = \DateTime::createFromFormat('d-m-Y', $dates);
            $begin = new \DateTime($a->format("Y-m-d") . " 00:00:00");
            $end = new \DateTime($b->format("Y-m-d") . " 23:59:59");
            $this->queryBuilder->Where('p.lastUpdate BETWEEN :from AND :to');
            $this->queryBuilder->setParameter('from', $begin);
            $this->queryBuilder->setParameter('to', $end);
        }
        $this->queryBuilder->orderBy('p.lastUpdate', 'DESC');
        $this->queryBuilder->setMaxResults(10);
        return $this->queryBuilder->getQuery()->getResult();
    }*/

    /**
     * @return Query
     */
    public function findAllVisible(Materiel $search): Query
    {
        $query = $this->createQueryBuilder('u');
        if ($search->getCodeImmo()) {
            $query = $query
            ->where('u.fullName like :code')
            ->setParameter('code', '%'.$search->getCodeImmo().'%');
        }
        return $query->getQuery();
    }

    public function findAllMatricule()
    {
        return $this->createQueryBuilder('p')
            ->select('p.matricule as matricule')
            ->orderBy('p.matricule','ASC')
            ->groupBy('p.matricule')
            ->getQuery()
            ->getResult();
    }

    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.matricule = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTotal()
    {
        $this->queryBuilder = $this->createQueryBuilder('u');
        $this->queryBuilder->select('count(u.id) as nb');
        $total = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $total[0]['nb'];
    }

}
