<?php

namespace App\Repository;

use App\Entity\Materiel;
use App\Entity\PropertySearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Materiel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Materiel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Materiel[]    findAll()
 * @method Materiel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterielRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Materiel::class);
    }

    // /**
    //  * @return Materiel[] Returns an array of Materiel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Materiel
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAll()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getListBE()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 1');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getListMoyen()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 2');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getListMauvais()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 3');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getListPanne()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 4');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getListHorsService()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 5');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getListPerdu()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 6');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getListEnStock()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 0');
        return $this->queryBuilder->getQuery()->getResult();
    }


    /**
     * @return Query
     */
    public function findAllVisible(PropertySearch $search): Query
    {
        $query = $this->createQueryBuilder('p');
        //$query->join('p.history','hist');
        //$query->where('hist.isActive = true');
        if ($search->getCode()) {
            $query->andWhere('p.codeImmo like :code or p.designation like :code');
            $query->setParameter('code', '%'.$search->getCode().'%');
        }
        if ($search->getResponsable()) {
            $query->andWhere('p.responsable like :resp');
            $query->setParameter('resp', '%'.$search->getResponsable().'%');
        }
        if ($search->getEtat()) {
            $query->andWhere('p.etat = :etat');
            $query->setParameter('etat', $search->getEtat());
        }
        return $query->getQuery();
    }

    public function findAllResponsable()
    {
        return $this->createQueryBuilder('p')
            ->select('p.responsable as responsable')
            ->orderBy('p.responsable','ASC')
            ->groupBy('p.responsable')
            ->getQuery()
            ->getResult();
    }

    public function findAllLieuAndService()
    {
        return $this->createQueryBuilder('p')
            ->select('p.lieuService as lieuService')
            ->orderBy('p.lieuService','ASC')
            ->groupBy('p.lieuService')
            ->getQuery()
            ->getResult();
    }

    public function findOneByCodeImmo($value): ?Materiel
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.codeImmo like :code')
            ->setParameter('code', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }


    //--------------COUNT------------------//

    public function getTotal()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $total = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $total[0]['nb'];
    }

    public function getStock()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 0');
        $fonction = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $fonction[0]['nb'];
    }

    public function getBE()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 1');
        $fonction = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $fonction[0]['nb'];
    }

    public function getMoyen()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 2');
        $mauvais = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $mauvais[0]['nb'];
    }

    public function getMauvais()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 3');
        $mauvais = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $mauvais[0]['nb'];
    }

    public function getPanne()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 4');
        $panne = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $panne[0]['nb'];
    }

    public function getHorsService()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 5');
        $nonSrv = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $nonSrv[0]['nb'];
    }

    public function getPerdu()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        //$this->queryBuilder->join('p.history','hist');
        //$this->queryBuilder->where('hist.isActive = true');
        $this->queryBuilder->andWhere('p.etat = 6');
        $nonSrv = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $nonSrv[0]['nb'];
    }



}
