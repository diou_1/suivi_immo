<?php

namespace App\Repository;

use App\Entity\History;
use App\Entity\Materiel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @method History|null find($id, $lockMode = null, $lockVersion = null)
 * @method History|null findOneBy(array $criteria, array $orderBy = null)
 * @method History[]    findAll()
 * @method History[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, History::class);
    }

    // /**
    //  * @return History[] Returns an array of History objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?History
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return Query
     */
    public function findAllVisible(Materiel $search): Query
    {
        $query = $this->createQueryBuilder('p');
        if ($search->getCodeImmo()) {
            $query = $query
            ->join('p.users','u')
            ->join('p.materiel','m')
            ->where('u.matricule like :code or u.fullName like :code')
            ->setParameter('code', '%'.$search->getCodeImmo().'%');
        }
        return $query->getQuery();
    }


    public function getTotal()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $total = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $total[0]['nb'];
    }

    public function isValide()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->where('p.isActive = true');
        $valide = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $valide[0]['nb'];
    }

    public function isRefuse()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->where('p.isActive = false');
        $annule = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $annule[0]['nb'];
    }


}
