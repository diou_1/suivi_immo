<?php

namespace App\Repository;

use App\Entity\Materiel;
use App\Entity\Proposition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Proposition|null find($id, $lockMode = null, $lockVersion = null)
 * @method Proposition|null findOneBy(array $criteria, array $orderBy = null)
 * @method Proposition[]    findAll()
 * @method Proposition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropositionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Proposition::class);
    }

    // /**
    //  * @return Proposition[] Returns an array of Proposition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Proposition
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAll()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->orderBy('p.createdAt', 'DESC');
        return $this->queryBuilder->getQuery()->getResult();
    }

    public function getFiveFirstPropositions()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->andWhere('p.isVisible = true');
        $this->queryBuilder->orderBy('p.createdAt', 'DESC');
        $this->queryBuilder->setMaxResults(5);
        return $this->queryBuilder->getQuery()->getResult();
    }

    /**
     * @return Query
     */
    public function findAllVisible(Materiel $search): Query
    {
        $query = $this->createQueryBuilder('p');
        $query->orderBy('p.createdAt','DESC');
        if ($search->getCodeImmo()) {
            $query->andWhere('p.designation like :code');
            $query->setParameter('code', '%'.$search->getCodeImmo().'%');
        }
        if ($search->getResponsable()) {
            $query->andWhere('p.responsable like :resp');
            $query->setParameter('resp', '%'.$search->getResponsable().'%');
        }
        if ($search->getLieuService()) {
            $query->andWhere('p.lieuService like :lieuS');
            $query->setParameter('lieuS', '%'.$search->getLieuService().'%');
        }
        return $query->getQuery();
    }

    public function findAllResponsable()
    {
        return $this->createQueryBuilder('p')
            ->select('p.responsable as responsable')
            ->orderBy('p.responsable','ASC')
            ->groupBy('p.responsable')
            ->getQuery()
            ->getResult();
    }

    public function findAllLieuAndService()
    {
        return $this->createQueryBuilder('p')
            ->select('p.lieuService as lieuService')
            ->orderBy('p.lieuService','ASC')
            ->groupBy('p.lieuService')
            ->getQuery()
            ->getResult();
    }

    public function findOneById($value): ?Proposition
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :key')
            ->setParameter('key', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTotal()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $total = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $total[0]['nb'];
    }

    public function getValide()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->where('p.isValide = true and p.isRefuse = false');
        $valide = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $valide[0]['nb'];
    }

    public function getEnAttente()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->where('p.isValide = false and p.isRefuse = false');
        $attente = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $attente[0]['nb'];
    }

    public function getRefuse()
    {
        $this->queryBuilder = $this->createQueryBuilder('p');
        $this->queryBuilder->select('count(p.id) as nb');
        $this->queryBuilder->where('p.isValide = false and p.isRefuse = true');
        $refuse = $this->queryBuilder->getQuery()->getScalarResult() == null ? [0, 0] : $this->queryBuilder->getQuery()->getScalarResult();
        return $refuse[0]['nb'];
    }

}
