<?php

namespace App\Form;

use App\Entity\Materiel;
use App\Entity\PropertySearch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertySearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Code immo',
                    'class' => 'form-control'
                ]
            ])
            ->add('responsable', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Responsable',
                    'class' => 'form-control'
                ]
            ])
            /*->add('responsable', EntityType::class, [
                'class'=>Materiel::class,
                'query_builder' => function (EntityRepository $er) {
                    //return $er->createQueryBuilder('u')->orderBy('u.responsable', 'ASC');
                    return $er->createQueryBuilder('p')
                    ->orderBy('p.responsable','ASC');
                }, 
                'choice_label' => 'responsable',
                'required' => false,
                'label' => false,
                'placeholder'=>'Responsable',
                'attr' => [
                    'class' => 'form-control show-tick'
                ]
            ])*/

            ->add('lieuService', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Lieu service',
                    'class' => 'form-control'
                ]
            ])
            ->add('etat', ChoiceType::class, [
                'required' => false,
                'label' => false,
                'choices' => $this->getChoices(),
                'attr' => [
                    'class' => 'form-control show-tick'
                ]
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertySearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function getChoices()
    {
        $choices = PropertySearch::ETAT;
        $output = [];
        foreach ($choices as $k => $v) {
            $output[$v] = $k;
        }
        return $output;
    }
}
