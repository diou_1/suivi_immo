<?php

namespace App\Form;

use App\Entity\PanneMateriel;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PanneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('datePanne')
            ->add('cause', TextareaType::class, ['label' => 'Cause', 'required'=>true, 'attr' => ['placeholder' => 'Entré  ici', 'class' => 'form-control']])
            ->add('solution', TextareaType::class, ['label' => 'Solution', 'required'=>true, 'attr' => ['placeholder' => 'Entré  ici', 'class' => 'form-control']])
            ->add('commentn1', TextareaType::class, ['label' => 'Commentaire N+1', 'required'=>false, 'attr' => ['placeholder' => 'Entré  ici', 'class' => 'form-control']])
            //->add('typePanne')
            //->add('isResolu')
            //->add('resoluDate')
            //->add('controlDate')
            //->add('isNonService')
            //->add('detailPanne')
            //->add('materiel')
            //->add('users')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PanneMateriel::class,
        ]);
    }
}
