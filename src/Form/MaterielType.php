<?php

namespace App\Form;

use App\Entity\Materiel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codeImmo', TextType::class, ['label' => 'Code Immo', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('nature', TextType::class, ['label' => 'Nature', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('designation', TextType::class, ['label' => 'Désignation', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('responsable', TextType::class, ['label' => 'Responsable', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('centreCout', TextType::class, ['label' => 'Centre du coût', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('lieuService', TextType::class, ['label' => 'Lieu/Service', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('dateFacture', DateType::class, ['label' => 'Date facture', 'widget' => 'single_text', 'html5' => false, 'format' => 'dd-MM-yyyy', 'attr' => ['placeholder' => 'dd-mm-yyyy', 'class' => 'form-control datepicker']])
            ->add('dateService', DateType::class, ['label' => 'Date de mise en service', 'widget' => 'single_text', 'html5' => false, 'format' => 'dd-MM-yyyy', 'attr' => ['placeholder' => 'dd-mm-yyyy', 'class' => 'form-control datepicker']])
            ->add('fournisseur', TextType::class, ['label' => 'Founisseur', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('valeurAchat', IntegerType::class, ['label' => 'Valeur achat (en Ariary)', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
            ->add('observation', TextareaType::class, ['label' => 'Observation', 'required'=>false, 'attr' => ['placeholder' => 'Champ facultatif', 'class' => 'form-control']])
            //->add('etat')
            //->add('editDate')
            //->add('history')
            //->add('users')
            //->add('userEdit')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Materiel::class,
        ]);
    }
}
