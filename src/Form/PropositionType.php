<?php

namespace App\Form;

use App\Entity\Proposition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation', TextType::class, ['label' => 'Désignation', 'attr' => ['placeholder' => '', 'class' => 'form-control']])
            ->add('qte', IntegerType::class, ['label' => 'Quantité', 'attr' => ['placeholder' => '', 'class' => 'form-control']])
            ->add('prixUnit', IntegerType::class, ['label' => 'Prix unitaire', 'attr' => ['placeholder' => '', 'class' => 'form-control']])
            ->add('centreCout', TextType::class, ['label' => 'Centre du coût', 'attr' => ['placeholder' => '', 'class' => 'form-control']])
            ->add('lieuService', TextType::class, ['label' => 'Lieu/Service', 'attr' => ['placeholder' => '', 'class' => 'form-control']])
            ->add('responsable', TextType::class, ['label' => 'Responsable', 'attr' => ['placeholder' => '', 'class' => 'form-control']])
            ->add('observations', TextareaType::class, ['label' => 'Observations', 'required'=>false, 'attr' => ['placeholder' => '', 'class' => 'form-control']])
            //->add('isValide')
            //->add('createdAt')
            //->add('controlAt')
            //->add('isVisible')
            //->add('isRefuse')
            //->add('users')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Proposition::class,
        ]);
    }
}
