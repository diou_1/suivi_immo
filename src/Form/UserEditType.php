<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('avatar', FileType::class, ['label' => 'Avatar','required'=>false, 'attr' => ['class' => 'form-control hidden', 'accept' => '.png,.jpg,.jpeg']])
        ->add('fullName', TextType::class, ['label' => 'Nom et Prenom', 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
        ->add('matricule', TextType::class, ['label' => 'Identifiant', 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
        ->add('service', TextType::class, ['label' => 'Service', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
        ->add('fonction', TextType::class, ['label' => 'Fonction', 'required'=>true, 'attr' => ['placeholder' => 'Champ obligatoire', 'class' => 'form-control']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
