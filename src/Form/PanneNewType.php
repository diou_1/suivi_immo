<?php

namespace App\Form;

use App\Entity\PanneMateriel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PanneNewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('datePanne')
            ->add('cause', TextareaType::class, ['label' => 'Cause', 'required'=>true, 'attr' => ['placeholder' => 'Entré  ici', 'class' => 'form-control']])
            ->add('solution', TextareaType::class, ['label' => 'Solution', 'required'=>true, 'attr' => ['placeholder' => 'Entré  ici', 'class' => 'form-control']])
            //->add('commentn1')
            ->add('typePanne', ChoiceType::class, [
                'choices' => $this->getChoices()
            ])
            //->add('isResolu')
            //->add('resoluDate')
            //->add('controlDate')
            //->add('isNonService')
            ->add('detailPanne', TextareaType::class, ['label' => 'Solution', 'required'=>true, 'attr' => ['placeholder' => 'Entré  ici', 'class' => 'form-control']])
            //->add('materiel')
            //->add('users')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PanneMateriel::class,
        ]);
    }

    public function getChoices()
    {
        $choices = PanneMateriel::TYPES;
        $output = [];
        foreach ($choices as $k => $v) {
            $output[$v] = $k;
        }
        return $output;
    }

}
