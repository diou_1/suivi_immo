<?php

namespace App\Entity;

use App\Repository\AgenceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AgenceRepository::class)
 */
class Agence
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Utilities\IdGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @ORM\OneToMany(targetEntity=Antenne::class, mappedBy="agence")
     */
    private $antennes;

    public function __construct()
    {
        $this->antennes = new ArrayCollection();
        $this->pannes = new ArrayCollection();
        $this->materiels = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return Collection|Antenne[]
     */
    public function getAntennes(): Collection
    {
        return $this->antennes;
    }

    public function addAntenne(Antenne $antenne): self
    {
        if (!$this->antennes->contains($antenne)) {
            $this->antennes[] = $antenne;
            $antenne->setAgence($this);
        }

        return $this;
    }

    public function removeAntenne(Antenne $antenne): self
    {
        if ($this->antennes->removeElement($antenne)) {
            // set the owning side to null (unless already changed)
            if ($antenne->getAgence() === $this) {
                $antenne->setAgence(null);
            }
        }

        return $this;
    }

    
}
