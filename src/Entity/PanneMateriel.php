<?php

namespace App\Entity;

use App\Repository\PanneMaterielRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PanneMaterielRepository::class)
 */
class PanneMateriel
{
    const TYPES = [
        '' => '- Choisissez un type -',
        'Matériel' => 'Matériel',
        'Système' => 'Système',
        'Logiciel' => 'Logiciel',
        'Autres' => 'Autres',
        'en cours de détection' => 'en cours de détection'
    ];

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Utilities\IdGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePanne;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cause;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $solution;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentn1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typePanne;

    /**
     * @ORM\ManyToOne(targetEntity=Materiel::class, inversedBy="panneMateriels")
     */
    private $materiel;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="pannes")
     */
    private $users;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isResolu;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $resoluDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $controlDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isNonService;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $detailPanne; 


    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDatePanne(): ?\DateTimeInterface
    {
        return $this->datePanne;
    }

    public function setDatePanne(\DateTimeInterface $datePanne): self
    {
        $this->datePanne = $datePanne;

        return $this;
    }

    public function getCause(): ?string
    {
        return $this->cause;
    }

    public function setCause(string $cause): self
    {
        $this->cause = $cause;

        return $this;
    }

    public function getSolution(): ?string
    {
        return $this->solution;
    }

    public function setSolution(string $solution): self
    {
        $this->solution = $solution;

        return $this;
    }

    public function getCommentn1(): ?string
    {
        return $this->commentn1;
    }

    public function setCommentn1(?string $commentn1): self
    {
        $this->commentn1 = $commentn1;

        return $this;
    }

    public function getTypePanne(): ?string
    {
        return $this->typePanne;
    }

    public function setTypePanne(string $typePanne): self
    {
        $this->typePanne = $typePanne;

        return $this;
    }

    public function getMateriel(): ?Materiel
    {
        return $this->materiel;
    }

    public function setMateriel(?Materiel $materiel): self
    {
        $this->materiel = $materiel;

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getIsResolu(): ?bool
    {
        return $this->isResolu;
    }

    public function setIsResolu(bool $isResolu): self
    {
        $this->isResolu = $isResolu;

        return $this;
    }

    public function getResoluDate(): ?\DateTimeInterface
    {
        return $this->resoluDate;
    }

    public function setResoluDate(?\DateTimeInterface $resoluDate): self
    {
        $this->resoluDate = $resoluDate;

        return $this;
    }

    public function getControlDate(): ?\DateTimeInterface
    {
        return $this->controlDate;
    }

    public function setControlDate(?\DateTimeInterface $controlDate): self
    {
        $this->controlDate = $controlDate;

        return $this;
    }

    public function getIsNonService(): ?bool
    {
        return $this->isNonService;
    }

    public function setIsNonService(bool $isNonService): self
    {
        $this->isNonService = $isNonService;

        return $this;
    }

    public function getDetailPanne(): ?string
    {
        return $this->detailPanne;
    }

    public function setDetailPanne(?string $detailPanne): self
    {
        $this->detailPanne = $detailPanne;

        return $this;
    }

}
