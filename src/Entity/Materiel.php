<?php

namespace App\Entity;

use App\Repository\MaterielRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MaterielRepository::class)
 */
class Materiel
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Utilities\IdGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $codeImmo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nature;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFacture;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateService;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $centreCout;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lieuService;

    /**
     * @ORM\OneToMany(targetEntity=PanneMateriel::class, mappedBy="materiel")
     */
    private $panneMateriels;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $responsable;

    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $observation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $valeurAchat;

    /**
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fournisseur;

    /**
     * @ORM\ManyToOne(targetEntity=History::class, inversedBy="materiel")
     */
    private $history;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="materiels")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="materielsEdit")
     */
    private $userEdit;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $editDate;

    public function __construct()
    {
        $this->panneMateriels = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCodeImmo(): ?string
    {
        return $this->codeImmo;
    }

    public function setCodeImmo(string $codeImmo): self
    {
        $this->codeImmo = $codeImmo;

        return $this;
    }

    public function getNature(): ?string
    {
        return $this->nature;
    }

    public function setNature(string $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    public function getDateFacture(): ?\DateTimeInterface
    {
        return $this->dateFacture;
    }

    public function setDateFacture(\DateTimeInterface $dateFacture): self
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    public function getDateService(): ?\DateTimeInterface
    {
        return $this->dateService;
    }

    public function setDateService(\DateTimeInterface $dateService): self
    {
        $this->dateService = $dateService;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(?string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getCentreCout(): ?string
    {
        return $this->centreCout;
    }

    public function setCentreCout(?string $centreCout): self
    {
        $this->centreCout = $centreCout;

        return $this;
    }

    public function getLieuService(): ?string
    {
        return $this->lieuService;
    }

    public function setLieuService(?string $lieuService): self
    {
        $this->lieuService = $lieuService;

        return $this;
    }

    /**
     * @return Collection|PanneMateriel[]
     */
    public function getPanneMateriels(): Collection
    {
        return $this->panneMateriels;
    }

    public function addPanneMateriel(PanneMateriel $panneMateriel): self
    {
        if (!$this->panneMateriels->contains($panneMateriel)) {
            $this->panneMateriels[] = $panneMateriel;
            $panneMateriel->setMateriel($this);
        }

        return $this;
    }

    public function removePanneMateriel(PanneMateriel $panneMateriel): self
    {
        if ($this->panneMateriels->removeElement($panneMateriel)) {
            // set the owning side to null (unless already changed)
            if ($panneMateriel->getMateriel() === $this) {
                $panneMateriel->setMateriel(null);
            }
        }

        return $this;
    }

    public function getResponsable(): ?string
    {
        return $this->responsable;
    }

    public function setResponsable(string $responsable): self
    {
        $this->responsable = $responsable;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(?string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

    public function getValeurAchat(): ?float
    {
        return $this->valeurAchat;
    }

    public function setValeurAchat(?float $valeurAchat): self
    {
        $this->valeurAchat = $valeurAchat;

        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getFournisseur(): ?string
    {
        return $this->fournisseur;
    }

    public function setFournisseur(?string $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    public function getHistory(): ?History
    {
        return $this->history;
    }

    public function setHistory(?History $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getUserEdit(): ?User
    {
        return $this->userEdit;
    }

    public function setUserEdit(?User $userEdit): self
    {
        $this->userEdit = $userEdit;

        return $this;
    }

    public function getEditDate(): ?\DateTimeInterface
    {
        return $this->editDate;
    }

    public function setEditDate(?\DateTimeInterface $editDate): self
    {
        $this->editDate = $editDate;

        return $this;
    }

}
