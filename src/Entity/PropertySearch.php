<?php

namespace App\Entity;

class PropertySearch
{
    const ETAT = [
        '' => 'Etat',
        '1' => 'BE',
        '2' => 'Moyen',
        '3' => 'Mauvais',
        '4' => 'Panne',
        '5' => 'Hors service',
        '6' => 'Perdu',
        '0' => 'En stock'
    ];

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $responsable;

    /**
     * @var string|null
     */
    private $lieuService;

    /**
     * @var string|null
     */
    private $etat;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function getResponsable(): ?int
    {
        return $this->responsable;
    }

    public function setResponsable(int $responsable): self
    {
        $this->responsable = $responsable;
        return $this;
    }

    public function getLieuService(): ?string
    {
        return $this->lieuService;
    }

    public function setLieuService(string $lieuService): self
    {
        $this->lieuService = $lieuService;
        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;
        return $this;
    }

}
