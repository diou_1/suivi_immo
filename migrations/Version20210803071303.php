<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210803071303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE proposition (id VARCHAR(255) NOT NULL, users_id VARCHAR(255) DEFAULT NULL, designation VARCHAR(255) NOT NULL, qte INT NOT NULL, prix_unit DOUBLE PRECISION DEFAULT NULL, centre_cout VARCHAR(255) DEFAULT NULL, lieu_service VARCHAR(255) DEFAULT NULL, responsable VARCHAR(255) NOT NULL, observations VARCHAR(255) DEFAULT NULL, is_valide BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_visible BOOLEAN NOT NULL, is_refuse BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C7CDC35367B3B43D ON proposition (users_id)');
        $this->addSql('ALTER TABLE proposition ADD CONSTRAINT FK_C7CDC35367B3B43D FOREIGN KEY (users_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE proposition');
    }
}
